/**
 * 
 */
$( document ).ready( function () {
	
	//Calendar registration - start
	$( "#SaveCalendar" ).click(function() {
		var $calendarID=$("#calendarID").val();
		var $calendarName=$("#calendarName").val();
		var $validityPeriod=$("#validityPeriod").val();
		var $location=$("#location").val();
		//
		if($("#userRegister").valid()){
			$.ajax({
	            url : "calendarSave.do",
	            type : "post",
	            data : {calendarID:$calendarID,calendarName:$calendarName,validityPeriod:$validityPeriod,location:$location},
	            success : function(data) {
	            	$('#successModal').modal('show');
	            },faliure : function(data) {
	            	$( "#failureModal" ).modal('show');
	            }
			});
		}
		//
		setTimeout(function() 
	    {
	       location.reload();  //Refresh page
	    }, 3000);
	});
	$( "#ResetCalendar" ).click(function() {
		var $calendarID=$("#calendarID").val('');
		var $calendarName=$("#calendarName").val('');
		var $validityPeriod=$("#validityPeriod").val('');
		var $location=$("#location").val('');
	});
	$( ".editCalendar" ).click(function() {
		var data=this.id.split("-");
		//$("#calendarID-"+data[1]).attr("readonly", false);
		$("#calendarName-"+data[1]).attr("readonly", false);
		$("#validityPeriod-"+data[1]).attr("readonly", false);
		$("#location-"+data[1]).prop("disabled", false);
		
		
	});
	$( ".updateCalendar" ).click(function() {
		var data=this.id.split("-");
		var $calendarID=$("#calendarID-"+data[1]).val();
		var $calendarName=$("#calendarName-"+data[1]).val();
		var $validityPeriod=$("#validityPeriod-"+data[1]).val();
		var $location=$("#location-"+data[1]).val();
		
		$("#calendarName-"+data[1]).attr("readonly", true);
		$("#validityPeriod-"+data[1]).attr("readonly", true);
		$("#location-"+data[1]).prop("disabled", true);
		
		if($.isNumeric($("#validityPeriod-"+data[1]).val())){
			 $('#updateModel').modal({ backdrop: 'static', keyboard: false })
	         .one('click', '#update', function() {
	        	 $.ajax({
	 				url : "updateCalendar.do",
	 	            type : "post",
	 	            data : {calendarID:$calendarID,calendarName:$calendarName,validityPeriod:$validityPeriod,location:$location},
	 	            success : function(data) {
	 	            	$('#updateSuccessModel').modal('show');
	 	            },faliure : function(data) {
	 	            	$( "#failureModal" ).modal('show');
	 	            }
	 			});
	         });
		}else{
			$( "#dataType" ).modal('show');
		}
		
	});
	
	$( ".deleteCalendar" ).click(function() {
		var data=this.id.split("-");
		var $calendarID=$("#calendarID-"+data[1]).val();
		var $calendarName=$("#calendarName-"+data[1]).val();
		var $validityPeriod=$("#validityPeriod-"+data[1]).val();
		var $location=$("#location-"+data[1]).val();
		
		$("#calendarName-"+data[1]).attr("readonly", true);
		$("#validityPeriod-"+data[1]).attr("readonly", true);
		$("#location-"+data[1]).prop("disabled", true);
		
		if($.isNumeric($("#validityPeriod-"+data[1]).val())){
			 $('#deleteModel').modal({ backdrop: 'static', keyboard: false })
	         .one('click', '#delete', function() {
	        	 $.ajax({
	 				url : "deleteCalendar.do",
	 	            type : "post",
	 	            data : {calendarID:$calendarID,calendarName:$calendarName,validityPeriod:$validityPeriod,location:$location},
	 	            success : function(data) {
	 	            	$('#deletedSuccessModel').modal('show');
	 	            	setTimeout(function() 
	 	            	{
	 	            		location.reload();  //Refresh page
	 	            	}, 3000);
	 	            },faliure : function(data) {
	 	            	$( "#failureModal" ).modal('show');
	 	            }
	 			});
	         });
		}else{
			$( "#dataType" ).modal('show');
		}
	});
	//Calendar registration - end
	
	//Holiday Registration - start
	$( ".editHoliday" ).click(function() {
		var data=this.id.split("-");
		$("#calendarName-"+data[1]).prop("disabled", false);
		$("#holidayDate-"+data[1]).attr("readonly", false);
		$("#holidayDesc-"+data[1]).attr("readonly", false);
	});
	
	
	$( ".updateHoliday" ).click(function() {
		var data=this.id.split("-");
		var $holidayID=data[1];
		var $calendarName=$("#calendarName-"+data[1]).val();
		var $validityPeriod=$("#holidayDate-"+data[1]).val();
		var $location=$("#holidayDesc-"+data[1]).val();
		
		$("#calendarName-"+data[1]).prop("disabled", true);
		$("#holidayDate-"+data[1]).attr("readonly", true);
		$("#holidayDesc-"+data[1]).attr("readonly", true);
		
		//if($.isNumeric($("#validityPeriod-"+data[1]).val())){
			 $('#updateModel').modal({ backdrop: 'static', keyboard: false })
	         .one('click', '#update', function() {
	        	 $.ajax({
	 				url : "updateHoliday.do",
	 	            type : "post",
	 	            data : {holidayID:$holidayID,location:$calendarName,datepicker:$validityPeriod,description:$location},
	 	            success : function(data) {
	 	            	//$('#updateSuccessModel').modal('show');
	 	            	location.reload(); 
	 	            	/*setTimeout(function() 
	 		 	            	{
	 		 	            		location.reload();  //Refresh page
	 		 	            	}, 2500);*/
	 	            },faliure : function(data) {
	 	            	$( "#failureModal" ).modal('show');
	 	            }
	 			});
	         });
		//}else{
		//	$( "#dataType" ).modal('show');
		//}
		
	});
	
	$( ".deleteHoliday" ).click(function() {
		var data=this.id.split("-");
		var $holidayID=data[1];
		var $calendarName=$("#calendarName-"+data[1]).val();
		var $validityPeriod=$("#holidayDate-"+data[1]).val();
		var $location=$("#holidayDesc-"+data[1]).val();
		
		$("#calendarName-"+data[1]).prop("disabled", true);
		$("#holidayDate-"+data[1]).attr("readonly", true);
		$("#holidayDesc-"+data[1]).attr("readonly", true);
		
		//if($.isNumeric($("#validityPeriod-"+data[1]).val())){
			 $('#deleteModel').modal({ backdrop: 'static', keyboard: false })
	         .one('click', '#delete', function() {
	        	 $.ajax({
	 				url : "deleteHoliday.do",
	 	            type : "post",
	 	            data : {holidayID:$holidayID,location:$calendarName,datepicker:$validityPeriod,description:$location},
	 	            success : function(data) {
	 	            	location.reload(); 
	 	            },faliure : function(data) {
	 	            	$( "#failureModal" ).modal('show');
	 	            }
	 			});
	         });
	});
	
	$("#SaveHoliday").click(function() {
		var $location=$("#location").val();
		var $datepicker=$("#datepicker").val();
		var $description=$("#description").val();
		if($("#userRegister").valid()){
			$.ajax({
				url : "holidaySave.do",
	            type : "post",
	            data : {location:$location,datepicker:$datepicker,description:$description},
	            success : function(data) {
	            	$('#successModal').modal('show');
	            },faliure : function(data) {
	            	$( "#failureModal" ).modal('show');
	            }
			});
	   }
		setTimeout(function() 
			    {
			       location.reload();  //Refresh page
			    }, 3000);
	});
	
	$("#ResetHoliday").click(function() {
		var $calendarID=$("#location").val('');
		var $datepicker=$("#datepicker").val('');
		var $description=$("#description").val('');
	});
	//Holiday Registration - end
	
	//verifier Registration - start
	$( ".editVerifier" ).click(function() {
		var data=this.id.split("-");
		$("#location-"+data[1]).prop("disabled", false);
		//$("#calendarID-"+data[1]).attr("readonly", false);
		$("#calendarName-"+data[1]).attr("readonly", false);
	});
	
	$( ".updateVerifier" ).click(function() {
		var data=this.id.split("-");
		//var $holidayID=data[1];
		var $calendarName=$("#calendarName-"+data[1]).val();
		var $calendarID=$("#calendarID-"+data[1]).val();
		var $location=$("#location-"+data[1]).val();
		
		$("#location-"+data[1]).prop("disabled", true);
		//$("#calendarID-"+data[1]).attr("readonly", true);
		$("#calendarName-"+data[1]).attr("readonly", true);
		
		//if($.isNumeric($("#validityPeriod-"+data[1]).val())){
			 $('#updateModel').modal({ backdrop: 'static', keyboard: false })
	         .one('click', '#update', function() {
	        	 $.ajax({
	 				url : "updateVerifier.do",
	 	            type : "post",
	 	            data : {calendarID:$calendarID,calendarName:$calendarName,location:$location},
	 	            success : function(data) {
	 	            	location.reload(); 
	 	            },faliure : function(data) {
	 	            	$( "#failureModal" ).modal('show');
	 	            }
	 			});
	         });
		//}else{
		//	$( "#dataType" ).modal('show');
		//}
		
	});
	
	$( ".deleteVerifier" ).click(function() {
		var data=this.id.split("-");
		//var $holidayID=data[1];
		var $calendarName=$("#calendarName-"+data[1]).val();
		var $calendarID=$("#calendarID-"+data[1]).val();
		var $location=$("#location-"+data[1]).val();
		
		$("#location-"+data[1]).prop("disabled", true);
		//$("#calendarID-"+data[1]).attr("readonly", true);
		$("#calendarName-"+data[1]).attr("readonly", true);
		
		//if($.isNumeric($("#validityPeriod-"+data[1]).val())){
			 $('#deleteModel').modal({ backdrop: 'static', keyboard: false })
	         .one('click', '#delete', function() {
	        	 $.ajax({
	 				url : "deleteVerifier.do",
	 	            type : "post",
	 	            data : {calendarID:$calendarID,calendarName:$calendarName,location:$location},
	 	            success : function(data) {
	 	            	location.reload(); 
	 	            },faliure : function(data) {
	 	            	$( "#failureModal" ).modal('show');
	 	            }
	 			});
	         });
		//}else{
		//	$( "#dataType" ).modal('show');
		//}
		
	});
	
	
	$("#SaveVerifier").click(function() {
		var $calendarID=$("#calendarID").val();
		var $calendarName=$("#calendarName").val();
		var $location=$("#location").val();
		if($("#userRegister").valid()){
			$.ajax({
				url : "verifierSave.do",
	            type : "post",
	            data : {calendarID:$calendarID,calendarName:$calendarName,location:$location},
	            success : function(data) {
	            	$('#successModal').modal('show');
	            },faliure : function(data) {
	            	$("#failureModal").modal('show');
	            }
			});
		}
		setTimeout(function() 
			    {
			       location.reload();  //Refresh page
			    }, 3000);
	});
	$("#ResetVerifier").click(function() {
		var $calendarID=$("#calendarID").val('');
		var $calendarName=$("#calendarName").val('');
		var $location=$("#location").val('');
	});
	//verifier Registration - end
	
	//verifier availability time registration - start
	$( ".editAvailability" ).click(function() {
		var data=this.id.split("-");
		/*var $verifier=$("#verifier-"+data[1]).val();
		var $calendar=$("#calendar-"+data[1]).val();
		var $datepicker=$("#datepicker-"+data[1]).val();
		var $fromHour=$("#fromHour-"+data[1]).val();
		var $fromMinute=$("#fromMinute-"+data[1]).val();
		var $toHour=$("#toHour-"+data[1]).val();
		var $toMinute=$("#toMinute-"+data[1]).val();*/
		
		$("#verifier-"+data[1]).prop("disabled", false);
		$("#calendar-"+data[1]).prop("disabled", false);
		$("#datepicker-"+data[1]).attr("readonly", false);
		$("#fromHour-"+data[1]).prop("disabled", false);
		$("#fromMinute-"+data[1]).prop("disabled", false);
		$("#toHour-"+data[1]).prop("disabled", false);
		$("#toMinute-"+data[1]).prop("disabled", false);
		
	});
	
	$( ".updateAvailability" ).click(function() {
		var data=this.id.split("-");
		var $verifier=$("#verifier-"+data[1]).val();
		var $calendar=$("#calendar-"+data[1]).val();
		var $datepicker=$("#datepicker-"+data[1]).val();
		var $fromHour=$("#fromHour-"+data[1]).val();
		var $fromMinute=$("#fromMinute-"+data[1]).val();
		var $toHour=$("#toHour-"+data[1]).val();
		var $toMinute=$("#toMinute-"+data[1]).val();
		var $availabilityId=data[1];
		
		if($("#userRegister").valid()){
			$.ajax({
	            url : "verifierAvailabilityUpdate.do",
	            type : "post",
	            data :{verifier:$verifier,calendar:$calendar,availabilityId:$availabilityId,datepicker:$datepicker,fromHour:$fromHour,fromMinute:$fromMinute,toHour:$toHour,toMinute:$toMinute},
	            success : function(data) {
	                //console.log(data)
	                if(data){
	                	for(var key in data){
	                		var value=data[key];
	                		if(value=='false'){
	                			$( "#failureModal" ).modal('show');
	                		}else{
	                			$('#successModal').modal('show');
	                		}
	                	}
	                }
	            } ,faliure : function(data) {
	            	if(data){
	            		for(var key in data){
	            			console.log(key,data[key]);
	                	}
	                }
	                $( "#failureModal" ).modal('show');
	            }
	        });
		}
		
		
	});
	
	$( ".deleteAvailability" ).click(function() {
		var data=this.id.split("-");
		var $verifier=$("#verifier-"+data[1]).val();
		var $calendar=$("#calendar-"+data[1]).val();
		var $datepicker=$("#datepicker-"+data[1]).val();
		var $fromHour=$("#fromHour-"+data[1]).val();
		var $fromMinute=$("#fromMinute-"+data[1]).val();
		var $toHour=$("#toHour-"+data[1]).val();
		var $toMinute=$("#toMinute-"+data[1]).val();
		var $availabilityId=data[1];
		
		if($("#userRegister").valid()){
			$.ajax({
	            url : "verifierAvailabilityDelete.do",
	            type : "post",
	            data :{verifier:$verifier,calendar:$calendar,availabilityId:$availabilityId,datepicker:$datepicker,fromHour:$fromHour,fromMinute:$fromMinute,toHour:$toHour,toMinute:$toMinute},
	            success : function(data) {
	                //console.log(data)
	                if(data){
	                	for(var key in data){
	                		var value=data[key];
	                		if(value=='false'){
	                			$( "#failureModal" ).modal('show');
	                		}else{
	                			$('#successModal').modal('show');
	                		}
	                	}
	                }
	            } ,faliure : function(data) {
	            	if(data){
	            		for(var key in data){
	            			console.log(key,data[key]);
	                	}
	                }
	                $( "#failureModal" ).modal('show');
	            }
	        });
		}
		
		
	});
	
	
	$( "#SaveAvailability" ).click(function() {
		var $verifier=$("#verifier").val();
		var $calendar=$("#calendar").val();
		var $datepicker=$("#datepicker").val();
		var	$fromHour=$("#fromHour").val();
		var	$fromMinute=$("#fromMinute").val();
		var	$toHour=$("#toHour").val();
		var	$toMinute=$("#toMinute").val();
		if($("#userRegister").valid()){
			$.ajax({
	            url : "verifierAvailabilitySave.do",
	            type : "post",
	            data :{verifier:$verifier,calendar:$calendar,datepicker:$datepicker,fromHour:$fromHour,fromMinute:$fromMinute,toHour:$toHour,toMinute:$toMinute},
	            success : function(data) {
	                //console.log(data)
	                if(data){
	                	for(var key in data){
	                		var value=data[key];
	                		if(value=='false'){
	                			$( "#failureModal" ).modal('show');
	                		}else{
	                			$('#successModal').modal('show');
	                		}
	                	}
	                }
	            } ,faliure : function(data) {
	            	if(data){
	            		for(var key in data){
	            			console.log(key,data[key]);
	                	}
	                }
	                $( "#failureModal" ).modal('show');
	            }
	        });
		}
		/*$.post('verifierAvailabilitySave.do', {verifier:$verifier,calendar:$calendar,datepicker:$datepicker,fromHour:$fromHour,fromMinute:$fromMinute,toHour:$toHour,toMinute:$toMinute}, 
			function(returnedData){
			         console.log(returnedData);
			}).fail(function(){
			      console.log("error");
		});*/
	});
	
	$("#ResetAvailability").click(function() {
		$("#verifier").val('');
		$("#calendar").val('');
		$("#datepicker").val('');
		$("#fromHour").val('');
		$("#fromMinute").val('');
		$("#toHour").val('');
		$("#toMinute").val('');
	});
	
	$( ".searchSlot" ).click(function() {
		var data=this.id.split("-");
		//var $holidayID=data[1];
		var $calendarName=$("#calendarName-"+data[1]).val();
		var $calendarID=$("#calendarID-"+data[1]).val();
		var $location=$("#location-"+data[1]).val();
		
		var $location=$("#location").val();
		var $calendar=$("#calendar").val();
		var $year=$("#year").val();
		var $month=$("#month").val();
		
		$("#location-"+data[1]).prop("disabled", true);
		//$("#calendarID-"+data[1]).attr("readonly", true);
		$("#calendarName-"+data[1]).attr("readonly", true);
		
		//if($.isNumeric($("#validityPeriod-"+data[1]).val())){
			 $('#updateModel').modal({ backdrop: 'static', keyboard: false })
	         .one('click', '#update', function() {
	        	 $.ajax({
	 				url : ".do",
	 	            type : "post",
	 	            data : {calendarID:$calendarID,calendarName:$calendarName,location:$location},
	 	            success : function(data) {
	 	            	$("#providersFormElementsTable").html("");
	 	            	location.reload(); 
	 	            },faliure : function(data) {
	 	            	$( "#failureModal" ).modal('show');
	 	            }
	 			});
	         });
		//}else{
		//	$( "#dataType" ).modal('show');
		//}
		
	});
	// verifier availability time registration - end
	$( ".editAvailabilityView" ).click(function() {
		var data=this.id.split("-");
		/*var $verifier=$("#verifier-"+data[1]).val();
		var $calendar=$("#calendar-"+data[1]).val();
		var $datepicker=$("#datepicker-"+data[1]).val();
		var $fromHour=$("#fromHour-"+data[1]).val();
		var $fromMinute=$("#fromMinute-"+data[1]).val();
		var $toHour=$("#toHour-"+data[1]).val();
		var $toMinute=$("#toMinute-"+data[1]).val();*/
		
		/*$("#verifier-"+data[1]).prop("disabled", false);
		$("#calendar-"+data[1]).prop("disabled", false);
		$("#datepicker-"+data[1]).attr("readonly", false);*/
		$("#fromHour-"+data[1]).prop("disabled", false);
		$("#fromMinute-"+data[1]).prop("disabled", false);
		$("#toHour-"+data[1]).prop("disabled", false);
		$("#toMinute-"+data[1]).prop("disabled", false);
		
	});
	
	$( ".updateAvailabilityView" ).click(function() {
		var data=this.id.split("-");
		var $verifier=$("#verifier-"+data[1]).val();
		var $calendar=$("#calendar-"+data[1]).val();
		var $datepicker=$("#datepicker-"+data[1]).val();
		var $fromHour=$("#fromHour-"+data[1]).val();
		var $fromMinute=$("#fromMinute-"+data[1]).val();
		var $toHour=$("#toHour-"+data[1]).val();
		var $toMinute=$("#toMinute-"+data[1]).val();
		var $scheduledId=data[1];
		var $customerId=data[2];
		
		if($("#userRegister").valid()){
			$.ajax({
	            url : "verifierSlotUpdate.do",
	            type : "post",
	            data :{verifier:$verifier,calendar:$calendar,scheduledId:$scheduledId,customerId:$customerId,datepicker:$datepicker,fromHour:$fromHour,fromMinute:$fromMinute,toHour:$toHour,toMinute:$toMinute},
	            success : function(data) {
	                //console.log(data)
	                if(data){
	                	for(var key in data){
	                		var value=data[key];
	                		if(value=='false'){
	                			$( "#failureModal" ).modal('show');
	                		}else{
	                			$('#successModal').modal('show');
	                		}
	                	}
	                }
	            } ,faliure : function(data) {
	            	if(data){
	            		for(var key in data){
	            			console.log(key,data[key]);
	                	}
	                }
	                $( "#failureModal" ).modal('show');
	            }
	        });
		}
		
		
	});
	
	$( ".deleteAvailabilityView" ).click(function() {
		var data=this.id.split("-");
		var $verifier=$("#verifier-"+data[1]).val();
		var $calendar=$("#calendar-"+data[1]).val();
		var $datepicker=$("#datepicker-"+data[1]).val();
		var $fromHour=$("#fromHour-"+data[1]).val();
		var $fromMinute=$("#fromMinute-"+data[1]).val();
		var $toHour=$("#toHour-"+data[1]).val();
		var $toMinute=$("#toMinute-"+data[1]).val();
		var $scheduledId=data[1];
		var $customerId=data[2];
		
		if($("#userRegister").valid()){
			$.ajax({
	            url : "verifierSlotDelete.do",
	            type : "post",
	            data :{verifier:$verifier,calendar:$calendar,scheduledId:$scheduledId,customerId:$customerId,datepicker:$datepicker,fromHour:$fromHour,fromMinute:$fromMinute,toHour:$toHour,toMinute:$toMinute},
	            success : function(data) {
	                //console.log(data)
	                if(data){
	                	for(var key in data){
	                		var value=data[key];
	                		if(value=='false'){
	                			$( "#failureModal" ).modal('show');
	                		}else{
	                			$('#successModal').modal('show');
	                		}
	                	}
	                }
	            } ,faliure : function(data) {
	            	if(data){
	            		for(var key in data){
	            			console.log(key,data[key]);
	                	}
	                }
	                $( "#failureModal" ).modal('show');
	            }
	        });
		}
		
		
	});
	
	//
	$(".address").click(function() {
		//var data=this.id.split("-");
		var data=this.id;
		data=data.replace("address-","");
		var $address=data;
		$('#addressModal').modal('show');
		$('#modalContent').show().html($address);
	});
	//
} );
