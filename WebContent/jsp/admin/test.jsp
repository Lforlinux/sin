<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>SingX | Calendar view</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description"
	content="Emerio Time Handling Information Center">
<meta name="author" content="Emerio">
<jsp:include page="/jsp/common/ethicCommon.jsp"/>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script type="text/javascript" src="js/jsonCall.js"></script>
<SCRIPT type="text/javascript">
    	function loadsValuesFromServlet(category) {
        �var $categoryValue=category;
       	 $.get('getSBUnits.do',{category:$categoryValue},function(responseJson) {�� 
       	 var $select = $('#subBusinessUnit');��
     	 $select.find('option').remove();���
        	$('<option>').val('').text('--- Select ---').appendTo($select);
        	���������������$.each($.parseJSON(responseJson), function(key, value) {�
        	�������������������$('<option>').val(key).text(value).appendTo($select);��
        	���������������});
        	});
    	}
</SCRIPT>
</head>
<head>
  <link rel="stylesheet" href="css/jquery.dataTables-1.min.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript" src="js/jquery.validate.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: "yy-mm-dd"
    });
  //loading years - start
	var min = new Date().getFullYear();
    max = min + 10;
    select = document.getElementById('year');

    for (var i = min; i<=max; i++){
	    var opt = document.createElement('option');
	    opt.value = i;
	    opt.innerHTML = i;
	    if(i=='${selectedYear}'){
	    	opt.selected='${selectedYear}';
	    }
	    select.appendChild(opt);
    }
	
    //loading years - end
    
    //loading month - start
    var d = new Date();
    var monthArray = new Array();
    monthArray[0] = "January";
    monthArray[1] = "February";
    monthArray[2] = "March";
    monthArray[3] = "April";
    monthArray[4] = "May";
    monthArray[5] = "June";
    monthArray[6] = "July";
    monthArray[7] = "August";
    monthArray[8] = "September";
    monthArray[9] = "October";
    monthArray[10] = "November";
    monthArray[11] = "December";
    for(m = 0; m <= 11; m++) {
        var optn = document.createElement("option");
        optn.text = monthArray[m];
        // server side month start from one
        //optn.value = (m+1);
     //alert(m.length);
        if(m<9){
        	optn.value = "0"+(m+1);
        	//console.log("zero==>"+optn.value);
        }else{
        	optn.value = (m+1);
        	//console.log(optn.value);
        }
        // if june selected
        /*if ( m == 5) {
            optn.selected = true;
        }*/
        if ( (m+1) == '${selectedMonth}') {
            optn.selected = true;
        }
        document.getElementById('month').options.add(optn);
    }
    
    //loading month - end
    //var modelAttributeValue = '${selectedLocation}';
    //alert("att==>"+modelAttributeValue);
    
    /* $('#myTable').DataTable(); */
  } );
  </script>
</head>
<body>
	<div class="navbar navbar-default" role="navigation">
		<div class="navbar-inner">
			<button type="button" class="navbar-toggle pull-left animated flip">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="welcomePage.jsp"> <!-- <img alt="Ethic Logo" src="img/ethic.png" class="hidden-xs"/> -->
				<span>SingX</span></a>
			<div class="btn-group pull-right">
				<button class="btn btn-default dropdown-toggle"
					data-toggle="dropdown">
					<i class="glyphicon glyphicon-user"></i><span
						class="hidden-sm hidden-xs"> ${userName}</span> <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<li><a href="homeLogin.do">Logout</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="ch-container">
		<div class="row">
			<!-- left menu starts -->
			<jsp:include page="/jsp/common/menu.jsp"/>
			<!--/span-->
			<!-- left menu ends -->

			<noscript>
				<div class="alert alert-block col-md-9">
					<h4 class="alert-heading">Warning!</h4>
					<p>
						You need to have <a href="http://en.wikipedia.org/wiki/JavaScript"
							target="_blank">JavaScript</a> enabled to use this site.
					</p>
				</div>
			</noscript>

			<div id="content" class="col-lg-10 col-sm-10">
				<div>
					<ul class="breadcrumb">
						<li><a href="welcomePage.jsp">Home</a></li>
						<li><a href="calendarView.do">Calendar View</a></li>
					</ul>
				</div>
				
				<div class="clearfix"></div>
				<br>
				<div class="box-inner">
					<div class="box-header well" data-original-title="">
						<h2>
							<i class="glyphicon glyphicon-user"></i> Calendar View
						</h2>
					</div>
					<!-- <div class="box-content"> -->
						<!-- <div class="row clearfix">
							<div class="col-md-12 column">
								<table class="table table-striped table-bordered responsive"
									id="tab_logic">
									
								</table>
							</div>
						</div> -->
			<div >
				<form id="userRegister" class="form-horizontal" action="verifierAvailabilitySlot.do" method="POST">
				<br/>
				<table id="tab_logic" style="min-width:350px">
					<%-- <tr id='addr0' >
						<td  >Verifier ID</td>
						<td><input id="calendarID" name="calendarID" type="text"
												class="form-control"  ><span style="color: red">${employeeId}</span></td>	
						<!-- <td style="color: red">Please enter the employee ID</td>	 -->					
					</tr>
					<tr id='addr0'>
						<td  >Verifier Name</td>
						<td><input id="calendarName" name="calendarName" type="text"
												class="form-control"   ><span style="color: red">${calendarName}</span></td>							
					</tr> --%>
					<%-- <tr id='addr0'>
						<td  >Validity Period</td>
						<td><input id="validityPeriod" name="validityPeriod" type="text"
												class="form-control"   ><span style="color: red">${validityPeriod}</span></td>							
					</tr> --%>
					<%-- <tr id='addr0'>
						<td  >User Name</td>
						<td><input id="userName" name="userName" type="text"
												class="form-control"   ><span style="color: red">${userName}</span></td>							
					</tr>
					<tr id='addr0'>
						<td  >Password</td>
						<td><input id="passwd" name="passwd" type="password"
												class="form-control"   ><span style="color: red">${passwd}</span></td>							
					</tr>
					<tr id='addr0'>
						<td  >Email</td>
						<td><input id="email" name="email" type="text"
												class="form-control"   ><span style="color: red">${email}</span></td>							
					</tr> --%>
					<tr id='addr0'>
						<td  >Country</td>
						<td>
							<select id="location" name="location" class="form-control">
											<option value="">--- Select ---</option>
											<c:forEach items="${locations}" var="location">
											<c:choose>
											        <%-- <option value="${location.key}">${location.value}</option> --%>
											        <c:when test="${location.key eq selectedLocation}">
															<option value="${location.key}" selected>${location.value}</option>
														</c:when>
													<c:otherwise>
															<option value="${location.key}">${location.value}</option>
													</c:otherwise>
											</c:choose>
											</c:forEach>
							</select><span style="color: red">${location}</span>
						</td>						
					</tr>
					<%-- <tr id='addr0'>
						<td  >Verifier</td>
						<td>
							<select name="verifier" id="verifier" class="form-control">
											<option value="">--- Select ---</option>
											<c:forEach items="${verifiers}" var="verifier">
											        <option value="${verifier.key}">${verifier.value}</option>
											</c:forEach>
							</select><span style="color: red">${verifier}</span>
						</td>						
					</tr> --%>
					<tr id='addr0'>
						<td  >Calendar</td>
						<td>
							<select name="calendar" id="calendar" class="form-control">
											<option value="">--- Select ---</option>
											<c:forEach items="${calendars}" var="calendar">
											<c:choose>
											       <%--  <option value="${calendar.key}">${calendar.value}</option> --%>
											        <c:when test="${calendar.key eq selectedCalendar}">
															<option value="${calendar.key}" selected>${calendar.value}</option>
													</c:when>
													<c:otherwise>
															<option value="${calendar.key}">${calendar.value}</option>
													</c:otherwise>
											 </c:choose>
											</c:forEach>
							</select><span style="color: red">${calendar}</span>
						</td>						
					</tr>
					<%-- <tr id='addr0'>
						<td  >Available Date</td>
						<td><input type="text" id="datepicker" name="datepicker" class="form-control"><span style="color: red">${validityPeriod}</span></td>							
					</tr> --%>
					<!-- Availablity time - start -->
					<tr id='addr0'>
						<td  >Period</td>
						<td>
							<!-- <div class="row"> -->
										<div class="col-sm-3">
											<div class="form-group">
												<select id="year" name="year" class="form-control">
													<!-- <option value="">hour</option> -->
											<%-- <c:forEach items="${fromHours}" var="fromHour">
											        <option value="${fromHour.key}">${fromHour.value}</option>
											</c:forEach>  --%>
												</select>
											</div>
										</div>
										<div class="col-sm-1">:</div>
										<div class="col-sm-4">
											<div class="form-group">
												<select id="month" name="month" class="form-control">
													<!-- <option value="">minute</option> -->
											<%-- <c:forEach items="${fromMinutes}" var="fromMinute">
											        <option value="${fromMinute.key}">${fromMinute.value}</option>
											</c:forEach> --%>
												</select>
											</div>
										</div> 
							<!-- </div> -->						
						</td>	
						
					</tr>
				<%-- 	<tr id='addr0'>
						<td  >Available To</td>
						<td>
							<!-- <div class="row"> -->
										<div class="col-sm-5">
											<div class="form-group">
												<select id="toHour" name="toHour" class="form-control">
													<option value="">hour</option>
											<c:forEach items="${toHours}" var="toHour">
											        <option value="${toHour.key}">${toHour.value}</option>
											</c:forEach>
												</select>
											</div>
										</div>
										<div class="col-sm-1">:</div>
										<div class="col-sm-5">
											<div class="form-group">
												<select id="toMinute" name="toMinute" class="form-control">
													<option value="">minute</option>
											<c:forEach items="${toMinutes}" var="toMinute">
											        <option value="${toMinute.key}">${toMinute.value}</option>
											</c:forEach>
												</select>
											</div>
										</div> 
							<!-- </div> -->		
						</td>	
						
					</tr> --%>
					<%-- <tr id='addr0'>
						<td  >Business Unit</td>
						<td><select id="businessUnit" name="businessUnit" class="form-control" onchange="loadsValuesFromServlet(this.value)">
											<option value="">--- Select ---</option>
											<c:forEach items="${businessUnits}" var="businessUnit">
											        <option value="${businessUnit.key}">${businessUnit.value}</option>
											</c:forEach>
							</select><span style="color: red">${BusinessUnitError}</span>
						</td>							
					</tr>
					<tr id='addr0'>
						<td  >Sub Business Unit</td>
						<td><select name="subBusinessUnit" id="subBusinessUnit" class="form-control subBusinessUnit">
											<option value="">--- Select ---</option>
							</select><span style="color: red">${subBusinessUnitError}</span>
						</td>						
					</tr> --%>
					<tr>
						<td></td>
					<td><!-- <button name="test" class="btn btn-primary">
								Save</button> --><input type = "submit" class="btn btn-primary" name="searchCalendar" id="searchCalendar" value="Search">&nbsp;&nbsp;
							<input type = "button" class="btn btn-primary" name="resetCalendar" id="resetCalendar" value="Reset"></td>
					</tr>
				</table>
							<div class="box-content">
								<table id="myTable" class="table table-striped table-bordered bootstrap-datatable responsive">
									<thead>
										<tr>
											<th>Verifier</th>
											<th>Customer Id</th>
											<th>Calendar</th>
											<th>Date</th>
											<th>Scheduled from</th>
											<th>Scheduled to</th>
											<th colspan="3" class="center">Action</th>
										</tr>
									</thead>
									<!-- <tbody>
										<tr>
											<td>-</td>
											<td>-</td>
											<td>-</td>
											<td class="center"><input type = "button" class="btn btn-primary" name="editCalendar" id="editCalendar" value="Edit"></td>
											<td class="center"><input type = "button" class="btn btn-primary" name="deleteCalendar" id="deleteCalendar" value="Delete"></td>
										</tr>
									</tbody> -->
									<tbody>
									<c:forEach items="${verifiersList}" var="calendar">
										<tr>
											<%-- <td><input id="calendarID-${calendar.verifierId}" name="calendarID" type="text" value="${calendar.verifierId}"
												class="form-control"  readonly></td>
											<td><input id="calendarName-${calendar.verifierId}" name="calendarName" type="text" value="${calendar.verifierName}"
												class="form-control" readonly></td> --%>
											<td>
												<select id="verifier-${calendar.scheduledId}" name="verifier" class="form-control" disabled>
												<option value="">--- Select ---</option>
												<c:forEach items="${verifiers}" var="location">
												<c:choose>
														<c:when test="${location.key eq calendar.verifierId}">
															<option value="${location.key}" selected>${location.value}</option>
														</c:when>
														<c:otherwise>
															<option value="${location.key}">${location.value}</option>
														</c:otherwise>
												</c:choose>
												</c:forEach>
												</select>
											</td>
											<td width="10%"><input type="text" id="customer-${calendar.customerId}" name="datepicker-${calendar.customerId}" value="${calendar.customerId}" class="form-control" readonly></td>	
											<%-- <td>${calendar.customerId}</td> --%>			
											<td>
												<select id="calendar-${calendar.scheduledId}" name="calendar" class="form-control" disabled>
												<option value="">--- Select ---</option>
												<c:forEach items="${calendars}" var="location">
												<c:choose>
														<c:when test="${location.key eq calendar.calendarId}">
															<option value="${location.key}" selected>${location.value}</option>
														</c:when>
														<c:otherwise>
															<option value="${location.key}">${location.value}</option>
														</c:otherwise>
												</c:choose>
												</c:forEach>
												</select>
											</td>
											<td width="11%"><input type="text" id="datepicker-${calendar.scheduledId}" name="datepicker-${calendar.scheduledId}" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${calendar.scheduledFrom}"/>" class="form-control datepick" readonly><span style="color: red">${validityPeriod}</span></td>										
											<td>
											<!-- <div class="row"> -->
														<div class="col-sm-5">
															<div class="form-group">
																<select id="fromHour-${calendar.scheduledId}" name="fromHour" class="form-control" disabled>
																	<option value="">hour</option>
															<c:forEach items="${fromHours}" var="fromHour">
																<fmt:formatDate value='${calendar.scheduledFrom}' var='startFormat'  pattern='HH'/>
															<c:choose>
																<c:when test="${fromHour.key eq startFormat}">
																	<option value="${fromHour.key}" selected>${fromHour.value}</option>
																</c:when>
																<c:otherwise>
																	<option value="${fromHour.key}">${fromHour.value}</option>
																</c:otherwise>
															</c:choose>
															</c:forEach> 
																</select>
															</div>
														</div>
														<div class="col-sm-1" style="padding-top: 8px; padding-right: 13px; padding-left: 9px;">:</div>
														<div class="col-sm-5">
															<div class="form-group">
																<select id="fromMinute-${calendar.scheduledId}" name="fromMinute" class="form-control" disabled>
																	<option value="">minute</option>
															<c:forEach items="${fromMinutes}" var="fromMinute">
															<fmt:formatDate value='${calendar.scheduledFrom}' var='startMinute'  pattern='mm'/>
															<c:choose>
																<c:when test="${fromMinute.key eq startMinute}">
																	<option value="${fromMinute.key}" selected>${fromMinute.value}</option>
																</c:when>
																<c:otherwise>
																	<option value="${fromMinute.key}">${fromMinute.value}</option>
																</c:otherwise>
															</c:choose>
															       <%--  <option value="${fromMinute.key}">${fromMinute.value}</option> --%>
															</c:forEach>
																</select>
															</div>
														</div> 
											<!-- </div> -->						
											</td>	
											<td>
											<!-- <div class="row"> -->
														<div class="col-sm-5">
															<div class="form-group">
																<select id="toHour-${calendar.scheduledId}" name="toHour" class="form-control" disabled>
																	<option value="">hour</option>
															<c:forEach items="${toHours}" var="toHour">
																<fmt:formatDate value='${calendar.scheduledTo}' var='endFormat'  pattern='HH'/>
															<c:choose>
																<c:when test="${toHour.key eq endFormat}">
																	<option value="${toHour.key}" selected>${toHour.value}</option>
																</c:when>
																<c:otherwise>
																	<option value="${toHour.key}">${toHour.value}</option>
																</c:otherwise>
															</c:choose>
															</c:forEach>
																</select>
															</div>
														</div>
														<div class="col-sm-1" style="padding-top: 8px; padding-right: 13px; padding-left: 9px;">:</div>
														<div class="col-sm-5">
															<div class="form-group">
																<select id="toMinute-${calendar.scheduledId}" name="toMinute" class="form-control" disabled>
																	<option value="">minute</option>
															<%-- <c:forEach items="${toMinutes}" var="toMinute">
															        <option value="${toMinute.key}">${toMinute.value}</option>
															</c:forEach> --%>
															<c:forEach items="${toMinutes}" var="toMinute">
															<fmt:formatDate value='${calendar.scheduledTo}' var='endMinute'  pattern='mm'/>
															<c:choose>
																<c:when test="${toMinute.key eq endMinute}">
																	<option value="${toMinute.key}" selected>${toMinute.value}</option>
																</c:when>
																<c:otherwise>
																	<option value="${toMinute.key}">${toMinute.value}</option>
																</c:otherwise>
															</c:choose>
															       <%--  <option value="${fromMinute.key}">${fromMinute.value}</option> --%>
															</c:forEach>
																</select>
															</div>
														</div> 
											<!-- </div> -->						
										</td>
											<td class="center"><input type = "button" class="btn btn-primary editAvailabilityView" name="editCalendar-${calendar.scheduledId}" id="editCalendar-${calendar.scheduledId}" value="Edit" ></td>
											<td class="center"><input type = "button" class="btn btn-primary updateAvailabilityView" name="updateCalendar-${calendar.scheduledId}" id="updateCalendar-${calendar.scheduledId}-${calendar.customerId}" value="Update"></td>
											<td class="center"><input type = "button" class="btn btn-primary deleteAvailabilityView" name="deleteCalendar-${calendar.scheduledId}" id="deleteCalendar-${calendar.scheduledId}-${calendar.customerId}" value="Delete"></td>
										</tr>
									</c:forEach>
									</tbody>
									</table>
							</div>
						</form>
				<br/>
			</div>
		</div>
	</div>
	</div>
	<hr>
	<!-- <footer class="row">
			<p class="col-md-9 col-sm-9 col-xs-12 copyright"> &nbsp;
				&copy; <a href="http://star.emeriocorp.com/India/default.aspx"
					target="_blank">Ethic</a> 2016 - 2017
			</p>

			<p class="col-md-3 col-sm-3 col-xs-12 powered-by">
				Powered by: <a href="http://star.emeriocorp.com/India/default.aspx">Emerio &nbsp;</a>
			</p>
		</footer> -->
	</div>
	
	<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- library for cookie management -->
	<script src="js/jquery.cookie.js"></script>
	<!-- calender plugin -->
	<script src='bower_components/moment/min/moment.min.js'></script>
	<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='js/jquery.dataTables.min.js'></script>

	<!-- select or dropdown enhancer -->
	<script src="bower_components/chosen/chosen.jquery.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
	<!-- notification plugin -->
	<script src="js/jquery.noty.js"></script>
	<!-- library for making tables responsive -->
	<script src="bower_components/responsive-tables/responsive-tables.js"></script>
	<!-- tour plugin -->
	<script
		src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
	<!-- star rating plugin -->
	<script src="js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="js/jquery.autogrow-textarea.js"></script>
	<!-- multiple file upload plugin -->
	<script src="js/jquery.uploadify-3.1.min.js"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<script src="js/charisma.js"></script>
	<script type="text/javascript">
		$( document ).ready( function () {
			/* $('#myTable').DataTable(); */
			$("#userRegister").validate( {
				rules: {
					calendarID:{
						required: true
					},
					calendarName:"required",
					validityPeriod:"required",
					userName: {
						required: true,
						maxlength: 20
					},
					passwd:"required",
					email: {
						required: true,
						email: true
					},
					location:"required",
					businessUnit:"required",
					subBusinessUnit:"required",
				},
				messages: {
					calendarID : {
						required:"Please enter the verifier ID"
						},
					calendarName:"Please enter the verifier name",
					validityPeriod:"Please enter the validity period",
					passwd:"Please enter the password",
					email: "Please enter a valid email address",
					location:"Please select the country",
					businessUnit:"Please select the business unit",
					subBusinessUnit:"Please select the sub business unit",
					userName: {
						required: "Please enter a username",
						maxlength: "username consist maxium of 20 characters only"
					}
				},
				errorPlacement: function ( error, element ) {
					error.addClass( "ui red pointing label transition" );
					error.insertAfter( element.parent() );
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".row" ).addClass( errorClass );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".row" ).removeClass( errorClass );
				}
			} );
		} );
	</script>
	<!-- <script type="text/javascript">
	    $(".form_datetime").datetimepicker({
	        format: "dd MM yyyy - HH:ii P",
	        showMeridian: true,
	        autoclose: true,
	        todayBtn: true
	    });
	</script>
	<div class="input-append date form_datetime" data-date="2012-12-21T15:25:00Z">
	    <input size="16" type="text" value="" readonly>
	    <span class="add-on"><i class="icon-remove"></i></span>
	    <span class="add-on"><i class="icon-th"></i></span>
	</div> -->
</body>
<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">�</button>
                    <h3>Status</h3>
                </div>
                <div class="modal-body">
                    <p align="center">Successfully inserted..</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                </div>
            </div>
        </div>
</div>
<div class="modal fade" id="failureModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">�</button>
                    <h3>Status</h3>
                </div>
                <div class="modal-body">
                    <p align="center">Failure in insertion..</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                </div>
            </div>
</div>
</div>
</html>
