<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>SingX | Verifier Registration</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description"
	content="Emerio Time Handling Information Center">
<meta name="author" content="Emerio">
<jsp:include page="/jsp/common/ethicCommon.jsp"/>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script type="text/javascript" src="js/jsonCall.js"></script>
<SCRIPT type="text/javascript">
    	function loadsValuesFromServlet(category) {
        �var $categoryValue=category;
       	 $.get('getSBUnits.do',{category:$categoryValue},function(responseJson) {�� 
       	 var $select = $('#subBusinessUnit');��
     	 $select.find('option').remove();���
        	$('<option>').val('').text('--- Select ---').appendTo($select);
        	���������������$.each($.parseJSON(responseJson), function(key, value) {�
        	�������������������$('<option>').val(key).text(value).appendTo($select);��
        	���������������});
        	});
    	}
</SCRIPT>

</head>
<body>
	<div class="navbar navbar-default" role="navigation">
		<div class="navbar-inner">
			<button type="button" class="navbar-toggle pull-left animated flip">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="welcomePage.jsp"> <!-- <img alt="Ethic Logo" src="img/ethic.png" class="hidden-xs"/> -->
				<span>singX</span></a>
			<div class="btn-group pull-right">
				<button class="btn btn-default dropdown-toggle"
					data-toggle="dropdown">
					<i class="glyphicon glyphicon-user"></i><span
						class="hidden-sm hidden-xs"> ${userName}</span> <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<li><a href="homeLogin.do">Logout</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="ch-container">
		<div class="row">
			<!-- left menu starts -->
			<jsp:include page="/jsp/common/menu.jsp"/>
			<!--/span-->
			<!-- left menu ends -->

			<noscript>
				<div class="alert alert-block col-md-9">
					<h4 class="alert-heading">Warning!</h4>
					<p>
						You need to have <a href="http://en.wikipedia.org/wiki/JavaScript"
							target="_blank">JavaScript</a> enabled to use this site.
					</p>
				</div>
			</noscript>

			<div id="content" class="col-lg-10 col-sm-10">
				<div>
					<ul class="breadcrumb">
						<li><a href="welcomePage.jsp">Home</a></li>
						<li><a href="calendarRegistration.do">Verifier Registration</a></li>
					</ul>
				</div>
				
				<div class="clearfix"></div>
				<br>
				<div class="box-inner">
					<div class="box-header well" data-original-title="">
						<h2>
							<i class="glyphicon glyphicon-user"></i> Verifier Registration
						</h2>
					</div>
					<!-- <div class="box-content"> -->
						<!-- <div class="row clearfix">
							<div class="col-md-12 column">
								<table class="table table-striped table-bordered responsive"
									id="tab_logic">
									
								</table>
							</div>
						</div> -->
			<div >
				<form id="userRegister" class="form-horizontal">
				<br/>
				<table id="tab_logic" style="min-width:350px">
					<tr id='addr0' >
						<td  >Verifier ID</td>
						<td><input id="calendarID" name="calendarID" type="text"
												class="form-control"  ><span style="color: red">${employeeId}</span></td>	
						<!-- <td style="color: red">Please enter the employee ID</td>	 -->					
					</tr>
					<tr id='addr0'>
						<td  >Verifier Name</td>
						<td><input id="calendarName" name="calendarName" type="text"
												class="form-control"   ><span style="color: red">${calendarName}</span></td>							
					</tr>
					<%-- <tr id='addr0'>
						<td  >Validity Period</td>
						<td><input id="validityPeriod" name="validityPeriod" type="text"
												class="form-control"   ><span style="color: red">${validityPeriod}</span></td>							
					</tr> --%>
					<%-- <tr id='addr0'>
						<td  >User Name</td>
						<td><input id="userName" name="userName" type="text"
												class="form-control"   ><span style="color: red">${userName}</span></td>							
					</tr>
					<tr id='addr0'>
						<td  >Password</td>
						<td><input id="passwd" name="passwd" type="password"
												class="form-control"   ><span style="color: red">${passwd}</span></td>							
					</tr>
					<tr id='addr0'>
						<td  >Email</td>
						<td><input id="email" name="email" type="text"
												class="form-control"   ><span style="color: red">${email}</span></td>							
					</tr> --%>
					<tr id='addr0'>
						<td  >Country</td>
						<td>
							<select id="location" name="location" class="form-control">
											<option value="">--- Select ---</option>
											<c:forEach items="${locations}" var="location">
											        <option value="${location.key}">${location.value}</option>
											</c:forEach>
							</select><span style="color: red">${location}</span>
						</td>						
					</tr>
					<!-- Availablity time - start -->
			<%-- 		<tr id='addr0'>
						<td  >From Time</td>
						<td>
							<!-- <div class="row"> -->
										<div class="col-sm-4">
											<div class="form-group">
												<select name="hour" name="hour" class="form-control">
													<option value="">hour</option>
													<c:forEach items="${locations}" var="location">
											        <option value="${location.key}">${location.value}</option>
											</c:forEach>
												</select>
											</div>
										</div>
										<div class="col-sm-1"></div>
										<div class="col-sm-4">
											<div class="form-group">
												<select name="hour" name="hour" class="form-control">
													<option value="">minute</option>
													<c:forEach items="${locations}" var="location">
											        <option value="${location.key}">${location.value}</option>
											</c:forEach>
												</select>
											</div>
										</div> 
							<!-- </div> -->						
						</td>	
						
					</tr>
					<tr id='addr0'>
						<td  >To Time</td>
						<td>
							<!-- <div class="row"> -->
										<div class="col-sm-4">
											<div class="form-group">
												<select name="hour" name="hour" class="form-control">
													<option value="">hour</option>
													<c:forEach items="${locations}" var="location">
											        <option value="${location.key}">${location.value}</option>
											</c:forEach>
												</select>
											</div>
										</div>
										<div class="col-sm-1"></div>
										<div class="col-sm-4">
											<div class="form-group">
												<select name="hour" name="hour" class="form-control">
													<option value="">minute</option>
													<c:forEach items="${locations}" var="location">
											        <option value="${location.key}">${location.value}</option>
											</c:forEach>
												</select>
											</div>
										</div> 
							<!-- </div> -->						
						</td>	
						
					</tr> --%>
					<!-- Availablity time - end -->
					<%-- <tr id='addr0'>
						<td  >Business Unit</td>
						<td><select id="businessUnit" name="businessUnit" class="form-control" onchange="loadsValuesFromServlet(this.value)">
											<option value="">--- Select ---</option>
											<c:forEach items="${businessUnits}" var="businessUnit">
											        <option value="${businessUnit.key}">${businessUnit.value}</option>
											</c:forEach>
							</select><span style="color: red">${BusinessUnitError}</span>
						</td>							
					</tr>
					<tr id='addr0'>
						<td  >Sub Business Unit</td>
						<td><select name="subBusinessUnit" id="subBusinessUnit" class="form-control subBusinessUnit">
											<option value="">--- Select ---</option>
							</select><span style="color: red">${subBusinessUnitError}</span>
						</td>						
					</tr> --%>
					<tr>
						<td></td>
						<td><!-- <button class="btn btn-primary">
								Save</button>&nbsp;&nbsp;
							<button class="btn btn-primary" type="reset" >
								Clear</button> -->
							<input type = "button" class="btn btn-primary" name="SaveVerifier" id="SaveVerifier" value="Save">&nbsp;&nbsp;
							<input type = "button" class="btn btn-primary" name="ResetVerifier" id="ResetVerifier" value="Reset"></td>
					</tr>
				</table>
				<div class="box-content">
								<table id="calendarData" class="table table-striped table-bordered bootstrap-datatable responsive">
									<thead>
										<tr>
											<th>verifier ID</th>
											<th>Verifier Name</th>
											<!-- <th>Validity Period</th> -->
											<th>Country</th>
											<th colspan="3" class="center">Action</th>
										</tr>
									</thead>
									<tbody>
									<c:forEach items="${verifiersList}" var="calendar">
										<tr>
											<td><input id="calendarID-${calendar.verifierId}" name="calendarID" type="text" value="${calendar.verifierId}"
												class="form-control"  readonly></td>
											<td><input id="calendarName-${calendar.verifierId}" name="calendarName" type="text" value="${calendar.verifierName}"
												class="form-control" readonly></td>
											<td>
												<select id="location-${calendar.verifierId}" name="location" class="form-control" disabled>
												<option value="">--- Select ---</option>
												<c:forEach items="${locations}" var="location">
												<c:choose>
														<c:when test="${location.key eq calendar.countryCode}">
															<option value="${location.key}" selected>${location.value}</option>
														</c:when>
														<c:otherwise>
															<option value="${location.key}">${location.value}</option>
														</c:otherwise>
												</c:choose>
												</c:forEach>
												</select>
											</td>
											<td class="center"><input type = "button" class="btn btn-primary editVerifier" name="editCalendar-${calendar.verifierId}" id="editCalendar-${calendar.verifierId}" value="Edit" ></td>
											<td class="center"><input type = "button" class="btn btn-primary updateVerifier" name="updateCalendar-${calendar.verifierId}" id="updateCalendar-${calendar.verifierId}" value="Update"></td>
											<td class="center"><input type = "button" class="btn btn-primary deleteVerifier" name="deleteCalendar-${calendar.verifierId}" id="deleteCalendar-${calendar.verifierId}" value="Delete"></td>
										</tr>
									</c:forEach>
									</tbody>
									</table>
							</div>
				</form>
				<br/>
			</div>
		</div>
	</div>
	</div>
	<hr>
	<!-- <footer class="row">
			<p class="col-md-9 col-sm-9 col-xs-12 copyright"> &nbsp;
				&copy; <a href="http://star.emeriocorp.com/India/default.aspx"
					target="_blank">Ethic</a> 2016 - 2017
			</p>

			<p class="col-md-3 col-sm-3 col-xs-12 powered-by">
				Powered by: <a href="http://star.emeriocorp.com/India/default.aspx">Emerio &nbsp;</a>
			</p>
		</footer> -->
	</div>
	
	<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- library for cookie management -->
	<script src="js/jquery.cookie.js"></script>
	<!-- calender plugin -->
	<script src='bower_components/moment/min/moment.min.js'></script>
	<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='js/jquery.dataTables.min.js'></script>

	<!-- select or dropdown enhancer -->
	<script src="bower_components/chosen/chosen.jquery.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
	<!-- notification plugin -->
	<script src="js/jquery.noty.js"></script>
	<!-- library for making tables responsive -->
	<script src="bower_components/responsive-tables/responsive-tables.js"></script>
	<!-- tour plugin -->
	<script
		src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
	<!-- star rating plugin -->
	<script src="js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="js/jquery.autogrow-textarea.js"></script>
	<!-- multiple file upload plugin -->
	<script src="js/jquery.uploadify-3.1.min.js"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<script src="js/charisma.js"></script>
	<script type="text/javascript">
		$( document ).ready( function () {
			$("#userRegister").validate( {
				rules: {
					calendarID:{
						required: true
					},
					calendarName:"required",
					validityPeriod:"required",
					userName: {
						required: true,
						maxlength: 20
					},
					passwd:"required",
					email: {
						required: true,
						email: true
					},
					location:"required",
					businessUnit:"required",
					subBusinessUnit:"required",
				},
				messages: {
					calendarID : {
						required:"Please enter the verifier ID"
						},
					calendarName:"Please enter the verifier name",
					validityPeriod:"Please enter the validity period",
					passwd:"Please enter the password",
					email: "Please enter a valid email address",
					location:"Please select the country",
					businessUnit:"Please select the business unit",
					subBusinessUnit:"Please select the sub business unit",
					userName: {
						required: "Please enter a username",
						maxlength: "username consist maxium of 20 characters only"
					}
				},
				errorPlacement: function ( error, element ) {
					error.addClass( "ui red pointing label transition" );
					error.insertAfter( element.parent() );
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".row" ).addClass( errorClass );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".row" ).removeClass( errorClass );
				}
			} );
		} );
	</script>
	<!-- <script type="text/javascript">
	    $(".form_datetime").datetimepicker({
	        format: "dd MM yyyy - HH:ii P",
	        showMeridian: true,
	        autoclose: true,
	        todayBtn: true
	    });
	</script>
	<div class="input-append date form_datetime" data-date="2012-12-21T15:25:00Z">
	    <input size="16" type="text" value="" readonly>
	    <span class="add-on"><i class="icon-remove"></i></span>
	    <span class="add-on"><i class="icon-th"></i></span>
	</div> -->
</body>

<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">�</button>
                    <h3>Status</h3>
                </div>
                <div class="modal-body">
                    <p align="center">Successfully inserted..</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                </div>
            </div>
        </div>
</div>
<div class="modal fade" id="failureModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">�</button>
                    <h3>Status</h3>
                </div>
                <div class="modal-body">
                    <p align="center">Failure in insertion..</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                </div>
            </div>
</div>
</div>
<div class="modal fade" id="updateModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">�</button>
                    <h3>Status</h3>
                </div>
                <div class="modal-body">
                    <p align="center">Are you sure want to update..</p>
                </div>
                <div class="modal-footer">
                    <!-- <a href="#" class="btn btn-default" data-dismiss="modal">Close</a> -->
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="update">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                </div>
            </div>
</div>
</div>
<div class="modal fade" id="deleteModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">�</button>
                    <h3>Status</h3>
                </div>
                <div class="modal-body">
                    <p align="center">Are you sure want to delete..</p>
                </div>
                <div class="modal-footer">
                    <!-- <a href="#" class="btn btn-default" data-dismiss="modal">Close</a> -->
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="delete">Delete</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                </div>
            </div>
</div>
</div>
</html>
