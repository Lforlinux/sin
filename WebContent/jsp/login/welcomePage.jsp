<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Ethic | Home</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description"
	content="Emerio Time Handling Information Center">
<meta name="author" content="Emerio">
 <jsp:include page="/jsp/common/includeStyle.jsp"/>
</head>

<body>
	<!-- topbar starts -->
	<div class="navbar navbar-default" role="navigation">
		<div class="navbar-inner">
			<button type="button" class="navbar-toggle pull-left animated flip">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="welcomePage.jsp"> <!-- <img alt="Ethic Logo" src="img/ethic.png" class="hidden-xs"/> -->
				<span>ETHIC</span></a>

			<!-- user dropdown starts -->
			<div class="btn-group pull-right">
				<button class="btn btn-default dropdown-toggle"
					data-toggle="dropdown">
					<i class="glyphicon glyphicon-user"></i><span
						class="hidden-sm hidden-xs"> ${userName}</span> <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<!-- <li><a href="#">Profile</a></li>
                    <li class="divider"></li> -->
					<li><a href="homeLogin.do">Logout</a></li>
				</ul>
			</div>
			<!-- user dropdown ends -->
		</div>
	</div>
	<!-- topbar ends -->
	<div class="ch-container">
		<div class="row">

			<!-- left menu starts -->
			<jsp:include page="/jsp/common/menu.jsp"/>
			<!--/span-->
			<!-- left menu ends -->

			<noscript>
				<div class="alert alert-block col-md-12">
					<h4 class="alert-heading">Warning!</h4>

					<p>
						You need to have <a href="http://en.wikipedia.org/wiki/JavaScript"
							target="_blank">JavaScript</a> enabled to use this site.
					</p>
				</div>
			</noscript>

			<div id="content" class="col-lg-10 col-sm-10">
				<!-- content starts -->
				<div>
					<ul class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li><a href="welcomePage.jsp">Dashboard</a></li>
					</ul>
				</div>
				
				<div class="row">
					<div class="box col-md-6">
						<div class="box-inner">
							<div class="box-header well">
								<h2>
									<i class="glyphicon glyphicon-user"></i> SAMPLE June Time Entry
									Completion (All People)
								</h2>
								<div class="box-icon">
									<a href="#" class="btn btn-minimize btn-round btn-default"><i
										class="glyphicon glyphicon-chevron-up"></i></a>
								</div>
							</div>
							<div class="box-content">
								<div id="calendar"></div>
							</div>
						</div>
					</div>
					<div class="box col-md-5">
						<div class="box-inner">
							<div class="box-header well">
								<h2>
									<i class="glyphicon glyphicon-user"></i> SAMPLE Recent
									Timesheet Status (My People)
								</h2>
								<div class="box-icon">
									<a href="#" class="btn btn-minimize btn-round btn-default"><i
										class="glyphicon glyphicon-chevron-up"></i></a>
								</div>
							</div>
							<div class="box-content">
								<table class="table">
									<thead>
										<tr>
											<th></th>
											<th>2 TIME SHEETS AGO</th>
											<th>LAST TIME SHEET</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><span class="label label-danger">REJECTED</span></td>
											<td style="text-align: center;">0</td>
											<td style="text-align: center;">0</td>
										</tr>
										<tr>
											<td><span class="label label-info">OPEN</span></td>
											<td style="text-align: center;">0</td>
											<td style="text-align: center;">0</td>
										</tr>
										<tr>
											<td><span class="label label-warning">WAITING</span></td>
											<td style="text-align: center;">0</td>
											<td style="text-align: center;">0</td>
										</tr>
										<tr>
											<td><span class="label label-success">APPROVED</span></td>
											<td style="text-align: center;">0</td>
											<td style="text-align: center;">0</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="box col-md-6">
						<div class="box-inner">
							<div class="box-header well">
								<h2>
									<i class="glyphicon glyphicon-list-alt"></i> Weekly status
								</h2>
								<div class="box-icon">
									<a href="#" class="btn btn-minimize btn-round btn-default"><i
										class="glyphicon glyphicon-chevron-up"></i></a>
								</div>
							</div>
							<div class="box-content">
								<div id="sincos" class="center" style="height: 300px"></div>
								<p id="hoverdata">
									Mouse position at (<span id="x">0</span>, <span id="y">0</span>).
									<span id="clickdata"></span>
								</p>
							</div>
						</div>
					</div>
					<div class="box col-md-5">
						<div class="box-inner">
							<div class="box-header well">
								<h2>
									<i class="glyphicon glyphicon-list-alt"></i> Weekly status - Chart
								</h2>
								<div class="box-icon">
									<a href="#" class="btn btn-minimize btn-round btn-default"><i
										class="glyphicon glyphicon-chevron-up"></i></a>
								</div>
							</div>
							<div class="box-content">
								<div id="piechart" style="height:300px"></div>
							</div>
						</div>
					</div>
				</div>


				<!-- chart libraries start -->
				<script src="bower_components/flot/excanvas.min.js"></script>
				<script src="bower_components/flot/jquery.flot.js"></script>
				<script src="bower_components/flot/jquery.flot.pie.js"></script>
				<script src="bower_components/flot/jquery.flot.stack.js"></script>
				<script src="bower_components/flot/jquery.flot.resize.js"></script>
				<!-- chart libraries end -->
				<script src="js/init-chart.js"></script>

				<!--/row-->
				<!-- content ends -->
			</div>
			<!--/#content.col-md-0-->
		</div>
		<!--/fluid-row-->


		<hr>

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">

			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">�</button>
						<h3>Settings</h3>
					</div>
					<div class="modal-body">
						<p>Here settings can be configured...</p>
					</div>
					<div class="modal-footer">
						<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
						<a href="#" class="btn btn-primary" data-dismiss="modal">Save
							changes</a>
					</div>
				</div>
			</div>
		</div>

		<footer class="row">
			<p class="col-md-9 col-sm-9 col-xs-12 copyright">
				&copy; <a href="http://star.emeriocorp.com/India/default.aspx"
					target="_blank">Ethic</a> 2016 - 2017
			</p>

			<p class="col-md-3 col-sm-3 col-xs-12 powered-by">
				Powered by: <a href="http://star.emeriocorp.com/India/default.aspx">Emerio</a>
			</p>
		</footer>

	</div>
	<!--/.fluid-container-->

	<!-- external javascript -->

	<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- library for cookie management -->
	<script src="js/jquery.cookie.js"></script>
	<!-- calender plugin -->
	<script src='bower_components/moment/min/moment.min.js'></script>
	<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='js/jquery.dataTables.min.js'></script>

	<!-- select or dropdown enhancer -->
	<script src="bower_components/chosen/chosen.jquery.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
	<!-- notification plugin -->
	<script src="js/jquery.noty.js"></script>
	<!-- library for making tables responsive -->
	<script src="bower_components/responsive-tables/responsive-tables.js"></script>
	<!-- tour plugin -->
	<script
		src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
	<!-- star rating plugin -->
	<script src="js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="js/jquery.autogrow-textarea.js"></script>
	<!-- multiple file upload plugin -->
	<script src="js/jquery.uploadify-3.1.min.js"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<script src="js/charisma.js"></script>


</body>
</html>
