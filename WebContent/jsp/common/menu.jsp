<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div class="col-sm-2 col-lg-2">
		<div class="sidebar-nav">
			<div class="nav-canvas">
				<div class="nav-sm nav nav-stacked"></div>
				<ul class="nav nav-pills nav-stacked main-menu">
					<li class="nav-header">Admin</li>
					<li><a class="ajax-link" href="calendarRegistration.do"><i
							class="glyphicon glyphicon-home"></i><span> Calendar Registration</span></a></li>
					<li><a class="ajax-link" href="holidayRegistration.do"><span> Holiday Registration</span></a></li>
					<li><a class="ajax-link" href="verifierRegistration.do"><span> Verifier Registration</span></a></li>
					<li><a class="ajax-link" href="availabilityRegistration.do"><span> Verifier Availability</span></a></li>
					<li><a class="ajax-link" href="calendarView.do"><span> Calendar View</span></a></li>
				</ul>
				<!-- <ul class="nav nav-pills nav-stacked main-menu">
					<li class="nav-header">User</li>
					<li><a class="ajax-link" href="user.do"><i
							class="glyphicon glyphicon-user"></i><span> Users</span></a></li>
					<li><a class="ajax-link" href="roleView.do"><i
							class="glyphicon glyphicon-user"></i><span> Roles</span></a></li>
					<li><a class="ajax-link" href="userRoleView.do"><i
							class="glyphicon glyphicon-user"></i><span> Users Roles</span></a></li>
				</ul>

				<ul class="nav nav-pills nav-stacked main-menu">
					<li class="nav-header">Time Sheet</li>
					<li><a class="ajax-link" href="timeSheet.do"><i
							class="glyphicon glyphicon-time"></i><span> Time Sheet
								Entry</span></a></li>
					<li><a class="ajax-link" href="timeSheetApproval.do"><i
							class="glyphicon glyphicon-time"></i><span> Time Sheet
								Approve/Reject</span></a></li>
					<li><a class="ajax-link" href="timeSheetApprovalView.do"><i
							class="glyphicon glyphicon-time"></i><span> Time Sheet
								Approve/Reject View</span></a></li>
					<li><a class="ajax-link" href="timeSheetDelete.do"><i
							class="glyphicon glyphicon-time"></i><span> Time Sheet
								Delete</span></a></li>
				</ul>

				<ul class="nav nav-pills nav-stacked main-menu">
					<li class="nav-header">Project</li>
					<li><a class="ajax-link" href="businessUnit.do"><i
							class="glyphicon glyphicon-edit"></i><span> BU</span></a></li>
					<li><a class="ajax-link" href="subBusinessUnit.do"><i
							class="glyphicon glyphicon-edit"></i><span> SBU</span></a></li>
					<li><a class="ajax-link" href="projectLists.do"><i
							class="glyphicon glyphicon-edit"></i><span> Project</span></a></li>
					<li><a class="ajax-link" href="category.do"><i
							class="glyphicon glyphicon-edit"></i><span> Category</span></a></li>
				</ul>

				<ul class="nav nav-pills nav-stacked main-menu">
					<li class="nav-header">General</li>					
					<li><a class="ajax-link" href="phase.do"><i
							class="glyphicon glyphicon-edit"></i><span> Phase</span></a></li>
					<li><a class="ajax-link" href="task.do"><i
							class="glyphicon glyphicon-edit"></i><span> Task</span></a></li>
					<li><a class="ajax-link" href="department.do"><i
							class="glyphicon glyphicon-edit"></i><span> Department</span></a></li>
					<li><a class="ajax-link" href="location.do"><i
							class="glyphicon glyphicon-edit"></i><span> Location</span></a></li>

				</ul>

				<ul class="nav nav-pills nav-stacked main-menu">
					<li class="nav-header">Report</li>
					<li><a class="ajax-link" href="reports.do"><i
							class="glyphicon glyphicon-edit"></i><span> User Report</span></a></li>
					<li><a class="ajax-link" href="reportsAdmin.do"><i
							class="glyphicon glyphicon-edit"></i><span> Admin Report</span></a></li>
					<li><a class="ajax-link" href="superAdminReport.do"><i
							class="glyphicon glyphicon-edit"></i><span> Super Admin</span></a></li>
				</ul> -->
			</div>
		</div>
	</div>
</body>
</html>