package com.singX.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import net.sf.json.JSONObject;

import com.singX.model.VerifcationDetails;

public class Util {

	public static boolean validateDate(String selectedDate){
		String[] formatStrings = { "yyyy-MM-dd hh:mm:ss"};

	    for (String formatString : formatStrings) {
	        try {
	            @SuppressWarnings("unused")
				Date date = new SimpleDateFormat(formatString).parse(selectedDate);
	            break;
	        } catch (ParseException e) {
	            return false;
	        }
	    }
	    return true;
	}
	
	public static boolean validateDateOnly(String selectedDate){
		String[] formatStrings = { "yyyy-MM-dd"};

	    for (String formatString : formatStrings) {
	        try {
	            @SuppressWarnings("unused")
				Date date = new SimpleDateFormat(formatString).parse(selectedDate);
	            break;
	        } catch (ParseException e) {
	            return false;
	        }
	    }
	    return true;
	}
	
	public static String convertdateTimetoDate(String dateTime) throws ParseException{
		SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Date selToDate=sdf3.parse(dateTime);
	    Calendar t=Calendar.getInstance();
	    t.setTime(selToDate);
	    return new SimpleDateFormat("yyyy-MM-dd").format(t.getTime());
	}
	
	public static JSONObject convertJSON(List<VerifcationDetails> registeredDetails){
		JSONObject json=new JSONObject();
		for(VerifcationDetails detail:registeredDetails){
			json.put("customerId",detail.getCustomerId());
			json.put("verifierId",detail.getVerifierId());
			json.put("selectedFromDateTime",detail.getScheduledFrom().toString());
			json.put("selectedToDateTime",detail.getScheduledTo().toString());
			json.put("bookedStatus", "true");
			json.put("calendarId",detail.getCalendarId());
			json.put("location", detail.getLocation());
		}
		return json;
	}
	
	public static Map<String,String> getCountries(){
		Map<String,String>country=new HashMap<String, String>();
		
		//get country name and country code - start
		String[] locales = Locale.getISOCountries();
		for (String countryCode : locales) {
		    Locale lc = new Locale("", countryCode);
		    //System.out.println("Country Code = " + lc.getCountry()+ ", Country Name = " + lc.getDisplayCountry(Locale.ENGLISH));
		    country.put(lc.getCountry(), lc.getDisplayCountry(Locale.ENGLISH));
		 }
		//get country name and country code - end
		//
		Map<String,String>sortedSet=new HashMap<String, String>();
		sortedSet=sortByValue(country);
		
		//
		return sortedSet;
	}
	
	public static String getCountryName(String code){
		String countryName="";
		Map<String,String>country=new HashMap<String, String>();
		//get country name and country code - start
		String[] locales = Locale.getISOCountries();
		for (String countryCode : locales) {
		    Locale lc = new Locale("", countryCode);
		    //System.out.println("Country Code = " + lc.getCountry()+ ", Country Name = " + lc.getDisplayCountry(Locale.ENGLISH));
		    country.put(lc.getCountry(), lc.getDisplayCountry(Locale.ENGLISH));
		 }
		//get country name and country code - end
		countryName=country.get(code);
		return countryName;
	}
	
	
	public static java.sql.Date getDatefromString(String date) throws ParseException{
		   SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		   Date parsedDate = dateFormat.parse(date);
		   java.sql.Date timestamp = new java.sql.Date(parsedDate.getTime());
		return timestamp;
	}
	
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> unsortMap) {

	    List<Map.Entry<K, V>> list =
	            new LinkedList<Map.Entry<K, V>>(unsortMap.entrySet());

	    Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
	        public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
	            return (o1.getValue()).compareTo(o2.getValue());
	        }
	    });

	    Map<K, V> result = new LinkedHashMap<K, V>();
	    for (Map.Entry<K, V> entry : list) {
	        result.put(entry.getKey(), entry.getValue());
	    }

	    return result;

	}
}
