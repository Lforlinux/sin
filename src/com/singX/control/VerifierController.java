package com.singX.control;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.singX.model.TimeSlotsAvailability;
import com.singX.model.VerifcationDetails;
import com.singX.model.VerifierDetails;
import com.singX.service.VerficationDetailsService;
import com.singX.util.Util;

@Controller
public class VerifierController {

	private VerficationDetailsService verficationDetailsService;
	
	@RequestMapping(value="/verifierRegistration.do",method = RequestMethod.GET)
	public ModelAndView verifierRegistration(Model model) throws IOException, ParseException {
		Map<String,String>country=new HashMap<String, String>();
		country=Util.getCountries();
		model.addAttribute("locations", country);
		
		List<VerifierDetails>verifiersList=verficationDetailsService.getAllVerifierList();
		model.addAttribute("verifiersList", verifiersList);
		
		return new ModelAndView("/admin/verifierRegistration");
	}
	
	@RequestMapping(value="/availabilityRegistration.do",method = RequestMethod.GET)
	public ModelAndView verifierAvailabilityReg(Model model) throws IOException, ParseException {
		/*Map<String,String>country=new HashMap<String, String>();
		country=Util.getCountries();*/
		
		Map<String,String>calendars=new HashMap<String, String>();
		calendars=verficationDetailsService.getCalendars();
		model.addAttribute("calendars", calendars);
		
		Map<String,String>verifiers=new HashMap<String, String>();
		verifiers=verficationDetailsService.getAllVerifiers();
		model.addAttribute("verifiers", verifiers);
		
		List<TimeSlotsAvailability> verifierList=verficationDetailsService.getAllVerifierAvailability();
		model.addAttribute("verifiersList", verifierList);
		
		Map<String,String>fromHours=new HashMap<String, String>();
		Map<String,String>fromMinutes=new HashMap<String, String>();
		Map<String,String>toHours=new HashMap<String, String>();
		Map<String,String>toMinutes=new HashMap<String, String>();
		
		fromHours=verficationDetailsService.getHours();
		fromMinutes=verficationDetailsService.getMinutes();
		toHours=verficationDetailsService.getHours();
		toMinutes=verficationDetailsService.getMinutes();
		
		model.addAttribute("fromHours", fromHours);
		model.addAttribute("fromMinutes", fromMinutes);
		model.addAttribute("toHours", toHours);
		model.addAttribute("toMinutes", toMinutes);
		
		return new ModelAndView("/admin/availabilityRegistration");
	}
	
	@RequestMapping(value="/availabilityUpdate.do",method = RequestMethod.GET)
	public ModelAndView verifierAvailabilityUpdate(Model model) throws IOException, ParseException {
		/*Map<String,String>country=new HashMap<String, String>();
		country=Util.getCountries();*/
		
		Map<String,String>calendars=new HashMap<String, String>();
		calendars=verficationDetailsService.getCalendars();
		model.addAttribute("calendars", calendars);
		
		Map<String,String>verifiers=new HashMap<String, String>();
		verifiers=verficationDetailsService.getAllVerifiers();
		model.addAttribute("verifiers", verifiers);
		
		List<VerifierDetails> verifierList=verficationDetailsService.getAllVerifierList();
		model.addAttribute("verifiersList", verifierList);
		
		Map<String,String>fromHours=new HashMap<String, String>();
		Map<String,String>fromMinutes=new HashMap<String, String>();
		Map<String,String>toHours=new HashMap<String, String>();
		Map<String,String>toMinutes=new HashMap<String, String>();
		
		fromHours=verficationDetailsService.getHours();
		fromMinutes=verficationDetailsService.getMinutes();
		toHours=verficationDetailsService.getHours();
		toMinutes=verficationDetailsService.getMinutes();
		
		model.addAttribute("fromHours", fromHours);
		model.addAttribute("fromMinutes", fromMinutes);
		model.addAttribute("toHours", toHours);
		model.addAttribute("toMinutes", toMinutes);
		
		return new ModelAndView("/admin/availabilityRegistration");
	}
	
	@RequestMapping(value="/verifierSave.do",method = RequestMethod.POST)
	public ModelAndView holidaySave(Model model,
			HttpServletRequest request) throws IOException, ParseException {
		String verifierId="";
		String verifierName="";
		String location="";
		if(request.getParameter("calendarID")!=null){
			verifierId=request.getParameter("calendarID");
		}
		if(request.getParameter("calendarName")!=null){
			verifierName=request.getParameter("calendarName");		
		}
		if(request.getParameter("location")!=null){
			location=request.getParameter("location");
		}
		//verficationDetailsService.addHolidaysIntoCalendar(calendarId, holidayDate, holidayDescription);
		verficationDetailsService.saveVerifier(verifierId, verifierName, location);
		return verifierRegistration(model);
	}
	
	@RequestMapping(value="/updateVerifier.do",method = RequestMethod.POST)
	public ModelAndView holidayUpdate(Model model,
			HttpServletRequest request) throws IOException, ParseException {
		String verifierId="";
		String verifierName="";
		String location="";
		if(request.getParameter("calendarID")!=null){
			verifierId=request.getParameter("calendarID");
		}
		if(request.getParameter("calendarName")!=null){
			verifierName=request.getParameter("calendarName");		
		}
		if(request.getParameter("location")!=null){
			location=request.getParameter("location");
		}
		//verficationDetailsService.addHolidaysIntoCalendar(calendarId, holidayDate, holidayDescription);
		verficationDetailsService.saveVerifier(verifierId, verifierName, location);
		return verifierRegistration(model);
	}
	
	@RequestMapping(value="/deleteVerifier.do",method = RequestMethod.POST)
	public ModelAndView holidayDelete(Model model,
			HttpServletRequest request) throws IOException, ParseException {
		String verifierId="";
		String verifierName="";
		String location="";
		if(request.getParameter("calendarID")!=null){
			verifierId=request.getParameter("calendarID");
		}
		if(request.getParameter("calendarName")!=null){
			verifierName=request.getParameter("calendarName");		
		}
		if(request.getParameter("location")!=null){
			location=request.getParameter("location");
		}
		//verficationDetailsService.addHolidaysIntoCalendar(calendarId, holidayDate, holidayDescription);
		verficationDetailsService.deleteVerifier(verifierId, verifierName, location);
		return verifierRegistration(model);
	}
	
	@RequestMapping(value="/verifierAvailabilitySave.do",method = RequestMethod.POST)
	public @ResponseBody JSONObject timeSlotAvailabilitySave(Model model,
			HttpServletRequest request) throws IOException, ParseException {
		/*String verifierId="";
		String verifierName="";
		String location="";
		if(request.getParameter("calendarID")!=null){
			verifierId=request.getParameter("calendarID");
		}
		if(request.getParameter("calendarName")!=null){
			verifierName=request.getParameter("calendarName");		
		}
		if(request.getParameter("location")!=null){
			location=request.getParameter("location");
		}
		//verficationDetailsService.addHolidaysIntoCalendar(calendarId, holidayDate, holidayDescription);
		verficationDetailsService.saveVerifier(verifierId, verifierName, location);*/
		//boolean status=false;
		JSONObject json=new JSONObject();
		String verifierId="";
		String calendarId="";
		String fromHour=request.getParameter("fromHour");
		String fromMinutes=request.getParameter("fromMinute");
		
		String toHour=request.getParameter("toHour");
		String toMinute=request.getParameter("toMinute");
		
		String availableDate=request.getParameter("datepicker");
		
		String fromDateTime=availableDate+" "+fromHour+":"+fromMinutes+":"+"00";
		String toDateTime=availableDate+" "+toHour+":"+toMinute+":"+"00";
		
		/*System.out.println("fromDateTime"+fromDateTime);
		System.out.println("toDateTime"+toDateTime);*/
		
		Timestamp availableFrom = Timestamp.valueOf(fromDateTime);
		Timestamp availableTo = Timestamp.valueOf(toDateTime);
		
		if(request.getParameter("verifier")!=null){
			verifierId=request.getParameter("verifier");
		}
		if(request.getParameter("calendar")!=null){
			calendarId=request.getParameter("calendar");
		}
		
		//System.out.println(ts);
		json=verficationDetailsService.saveTimeSlotAvailability(verifierId, calendarId, availableFrom, availableTo,availableDate,fromDateTime,toDateTime);
		/*status=true;
		json.put("message",status);*/
		return json;
	}
	
	@RequestMapping(value="/verifierAvailabilityUpdate.do",method = RequestMethod.POST)
	public @ResponseBody JSONObject timeSlotAvailabilityUpdate(Model model,
			HttpServletRequest request) throws IOException, ParseException {
		/*String verifierId="";
		String verifierName="";
		String location="";
		if(request.getParameter("calendarID")!=null){
			verifierId=request.getParameter("calendarID");
		}
		if(request.getParameter("calendarName")!=null){
			verifierName=request.getParameter("calendarName");		
		}
		if(request.getParameter("location")!=null){
			location=request.getParameter("location");
		}
		//verficationDetailsService.addHolidaysIntoCalendar(calendarId, holidayDate, holidayDescription);
		verficationDetailsService.saveVerifier(verifierId, verifierName, location);*/
		//boolean status=false;
		JSONObject json=new JSONObject();
		String verifierId="";
		String calendarId="";
		String availabilityId="";
		String fromHour=request.getParameter("fromHour");
		String fromMinutes=request.getParameter("fromMinute");
		
		String toHour=request.getParameter("toHour");
		String toMinute=request.getParameter("toMinute");
		
		String availableDate=request.getParameter("datepicker");
		
		String fromDateTime=availableDate+" "+fromHour+":"+fromMinutes+":"+"00";
		String toDateTime=availableDate+" "+toHour+":"+toMinute+":"+"00";
		
		/*System.out.println("fromDateTime"+fromDateTime);
		System.out.println("toDateTime"+toDateTime);*/
		
		Timestamp availableFrom = Timestamp.valueOf(fromDateTime);
		Timestamp availableTo = Timestamp.valueOf(toDateTime);
		
		if(request.getParameter("verifier")!=null){
			verifierId=request.getParameter("verifier");
		}
		if(request.getParameter("calendar")!=null){
			calendarId=request.getParameter("calendar");
		}
		if(request.getParameter("availabilityId")!=null){
			availabilityId=request.getParameter("availabilityId");
		}
		//System.out.println(ts);
		json=verficationDetailsService.updateTimeSlotAvailability(verifierId, calendarId,availabilityId,availableFrom, availableTo,availableDate,fromDateTime,toDateTime);
		/*status=true;
		json.put("message",status);*/
		return json;
	}

	@RequestMapping(value="/verifierAvailabilityDelete.do",method = RequestMethod.POST)
	public @ResponseBody JSONObject timeSlotAvailabilityDelete(Model model,
			HttpServletRequest request) throws IOException, ParseException {
		/*String verifierId="";
		String verifierName="";
		String location="";
		if(request.getParameter("calendarID")!=null){
			verifierId=request.getParameter("calendarID");
		}
		if(request.getParameter("calendarName")!=null){
			verifierName=request.getParameter("calendarName");		
		}
		if(request.getParameter("location")!=null){
			location=request.getParameter("location");
		}
		//verficationDetailsService.addHolidaysIntoCalendar(calendarId, holidayDate, holidayDescription);
		verficationDetailsService.saveVerifier(verifierId, verifierName, location);*/
		//boolean status=false;
		JSONObject json=new JSONObject();
		String verifierId="";
		String calendarId="";
		String availabilityId="";
		String fromHour=request.getParameter("fromHour");
		String fromMinutes=request.getParameter("fromMinute");
		
		String toHour=request.getParameter("toHour");
		String toMinute=request.getParameter("toMinute");
		
		String availableDate=request.getParameter("datepicker");
		
		String fromDateTime=availableDate+" "+fromHour+":"+fromMinutes+":"+"00";
		String toDateTime=availableDate+" "+toHour+":"+toMinute+":"+"00";
		
		/*System.out.println("fromDateTime"+fromDateTime);
		System.out.println("toDateTime"+toDateTime);*/
		
		Timestamp availableFrom = Timestamp.valueOf(fromDateTime);
		Timestamp availableTo = Timestamp.valueOf(toDateTime);
		
		if(request.getParameter("verifier")!=null){
			verifierId=request.getParameter("verifier");
		}
		if(request.getParameter("calendar")!=null){
			calendarId=request.getParameter("calendar");
		}
		if(request.getParameter("availabilityId")!=null){
			availabilityId=request.getParameter("availabilityId");
		}
		//System.out.println(ts);
		json=verficationDetailsService.deleteTimeSlotAvailability(verifierId, calendarId,availabilityId,availableFrom, availableTo,availableDate,fromDateTime,toDateTime);
		/*status=true;
		json.put("message",status);*/
		return json;
	}
	
	@RequestMapping(value="/calendarView.do",method = RequestMethod.GET)
	public ModelAndView calculatorView(Model model) throws IOException, ParseException {
		//ModelAndView mv=new ModelAndView();
		Map<String,String>country=new HashMap<String, String>();
		country=Util.getCountries();
		model.addAttribute("locations", country);
		
		Map<String,String>calendars=new HashMap<String, String>();
		calendars=verficationDetailsService.getCalendars();
		model.addAttribute("calendars", calendars);
		
		return new ModelAndView("/admin/calendarView");
	}
	
	@RequestMapping(value="/verifierAvailabilitySlot.do",method = RequestMethod.POST)
	public ModelAndView searchCalendar (Model model,
			HttpServletRequest request) throws IOException, ParseException {
		//JSONObject json=new JSONObject();
		String year="";
		String month="";
		String fromDate="";
		String calendarId="";
		String selectedLocation="";
		if(request.getParameter("calendar")!=null){
			calendarId=request.getParameter("calendar");
		}
		if(request.getParameter("year")!=null){
			year=request.getParameter("year");
		}
		if(request.getParameter("month")!=null){
			month=request.getParameter("month");
		}
		if(request.getParameter("location")!=null){
			selectedLocation=request.getParameter("location");
		}
		fromDate=year+"-"+month;
		List<VerifcationDetails> verifiersList=verficationDetailsService.getAllSlots(fromDate,calendarId);
		model.addAttribute("verifiersList", verifiersList);
		
		Map<String,String>fromHours=new HashMap<String, String>();
		Map<String,String>fromMinutes=new HashMap<String, String>();
		Map<String,String>toHours=new HashMap<String, String>();
		Map<String,String>toMinutes=new HashMap<String, String>();
		
		fromHours=verficationDetailsService.getHours();
		fromMinutes=verficationDetailsService.getMinutes();
		toHours=verficationDetailsService.getHours();
		toMinutes=verficationDetailsService.getMinutes();
		
		model.addAttribute("fromHours", fromHours);
		model.addAttribute("fromMinutes", fromMinutes);
		model.addAttribute("toHours", toHours);
		model.addAttribute("toMinutes", toMinutes);
		
		Map<String,String>verifiers=new HashMap<String, String>();
		verifiers=verficationDetailsService.getAllVerifiers();
		model.addAttribute("verifiers", verifiers);
		model.addAttribute("selectedLocation", selectedLocation);
		model.addAttribute("selectedCalendar", calendarId);
		model.addAttribute("selectedMonth",month);
		model.addAttribute("selectedYear",year);
		/*Map<String,String>country=new HashMap<String, String>();
		country=Util.getCountries();
		model.addAttribute("locations", country);
		
		Map<String,String>calendars=new HashMap<String, String>();
		calendars=verficationDetailsService.getCalendars();
		model.addAttribute("calendars", calendars);*/
		
		return calculatorView(model);
	}
	
	@RequestMapping(value="/verifierSlotUpdate.do",method = RequestMethod.POST)
	public @ResponseBody JSONObject timeSlotUpdate(Model model,
			HttpServletRequest request) throws IOException, ParseException {
		/*String verifierId="";
		String verifierName="";
		String location="";
		if(request.getParameter("calendarID")!=null){
			verifierId=request.getParameter("calendarID");
		}
		if(request.getParameter("calendarName")!=null){
			verifierName=request.getParameter("calendarName");		
		}
		if(request.getParameter("location")!=null){
			location=request.getParameter("location");
		}
		//verficationDetailsService.addHolidaysIntoCalendar(calendarId, holidayDate, holidayDescription);
		verficationDetailsService.saveVerifier(verifierId, verifierName, location);*/
		//boolean status=false;
		JSONObject json=new JSONObject();
		String verifierId="";
		String calendarId="";
		String scheduledId="";
		String customerId="";
		String fromHour=request.getParameter("fromHour");
		String fromMinutes=request.getParameter("fromMinute");
		
		String toHour=request.getParameter("toHour");
		String toMinute=request.getParameter("toMinute");
		
		String availableDate=request.getParameter("datepicker");
		
		String fromDateTime=availableDate+" "+fromHour+":"+fromMinutes+":"+"00";
		String toDateTime=availableDate+" "+toHour+":"+toMinute+":"+"00";
		
		/*System.out.println("fromDateTime"+fromDateTime);
		System.out.println("toDateTime"+toDateTime);*/
		
		Timestamp availableFrom = Timestamp.valueOf(fromDateTime);
		Timestamp availableTo = Timestamp.valueOf(toDateTime);
		
		if(request.getParameter("verifier")!=null){
			verifierId=request.getParameter("verifier");
		}
		if(request.getParameter("calendar")!=null){
			calendarId=request.getParameter("calendar");
		}
		if(request.getParameter("scheduledId")!=null){
			scheduledId=request.getParameter("scheduledId");
		}
		if(request.getParameter("customerId")!=null){
			customerId=request.getParameter("customerId");
		}
		//System.out.println(ts);
		json=verficationDetailsService.updateSlotAvailability(verifierId, calendarId,scheduledId,customerId,availableFrom, availableTo,availableDate,fromDateTime,toDateTime);
		/*status=true;
		json.put("message",status);*/
		return json;
	}
	
	@RequestMapping(value="/verifierSlotDelete.do",method = RequestMethod.POST)
	public @ResponseBody JSONObject timeSlotDelete(Model model,
			HttpServletRequest request) throws IOException, ParseException {
		/*String verifierId="";
		String verifierName="";
		String location="";
		if(request.getParameter("calendarID")!=null){
			verifierId=request.getParameter("calendarID");
		}
		if(request.getParameter("calendarName")!=null){
			verifierName=request.getParameter("calendarName");		
		}
		if(request.getParameter("location")!=null){
			location=request.getParameter("location");
		}
		//verficationDetailsService.addHolidaysIntoCalendar(calendarId, holidayDate, holidayDescription);
		verficationDetailsService.saveVerifier(verifierId, verifierName, location);*/
		//boolean status=false;
		JSONObject json=new JSONObject();
		String verifierId="";
		String calendarId="";
		String scheduledId="";
		String customerId="";
		String fromHour=request.getParameter("fromHour");
		String fromMinutes=request.getParameter("fromMinute");
		
		String toHour=request.getParameter("toHour");
		String toMinute=request.getParameter("toMinute");
		
		String availableDate=request.getParameter("datepicker");
		
		String fromDateTime=availableDate+" "+fromHour+":"+fromMinutes+":"+"00";
		String toDateTime=availableDate+" "+toHour+":"+toMinute+":"+"00";
		
		/*System.out.println("fromDateTime"+fromDateTime);
		System.out.println("toDateTime"+toDateTime);*/
		
		Timestamp availableFrom = Timestamp.valueOf(fromDateTime);
		Timestamp availableTo = Timestamp.valueOf(toDateTime);
		
		if(request.getParameter("verifier")!=null){
			verifierId=request.getParameter("verifier");
		}
		if(request.getParameter("calendar")!=null){
			calendarId=request.getParameter("calendar");
		}
		if(request.getParameter("scheduledId")!=null){
			scheduledId=request.getParameter("scheduledId");
		}
		if(request.getParameter("customerId")!=null){
			customerId=request.getParameter("customerId");
		}
		//System.out.println(ts);
		json=verficationDetailsService.deleteSlotAvailability(verifierId, calendarId,scheduledId,customerId,availableFrom, availableTo,availableDate,fromDateTime,toDateTime);
		/*status=true;
		json.put("message",status);*/
		return json;
	}

	@Autowired
	public void setVerficationDetailsService(
			VerficationDetailsService verficationDetailsService) {
		this.verficationDetailsService = verficationDetailsService;
	}
	
	
}
