package com.singX.control;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.singX.model.HolidayList;
import com.singX.service.HolidayService;

@Controller
public class HolidayController {
	
	private HolidayService holidayService;
	
	@RequestMapping(value="/holidayRegistration.do",method = RequestMethod.GET)
	public ModelAndView holidayRegistration(Model model) throws IOException, ParseException {
		Map<String,String>calendars=new HashMap<String, String>();
		calendars=holidayService.getCalendars();
		model.addAttribute("locations", calendars);
		
		List<HolidayList> calendarsList=holidayService.getAllCalendars();
		model.addAttribute("calendarsList", calendarsList);
		
		return new ModelAndView("/admin/holidayRegistration");
	}

	@RequestMapping(value="/holidaySave.do",method = RequestMethod.POST)
	public ModelAndView holidaySave(Model model,
			HttpServletRequest request) throws IOException, ParseException {
		String calendarId="";
		String holidayDate="";
		String holidayDescription="";
		if(request.getParameter("location")!=null){
			calendarId=request.getParameter("location");
		}
		if(request.getParameter("datepicker")!=null){
			holidayDate=request.getParameter("datepicker");		
		}
		if(request.getParameter("description")!=null){
			holidayDescription=request.getParameter("description");
		}
		holidayService.addHolidaysIntoCalendar(calendarId, holidayDate, holidayDescription);
		return holidayRegistration(model);
	}
	
	@RequestMapping(value="/updateHoliday.do",method = RequestMethod.POST)
	public ModelAndView holidayUpdate(Model model,
			HttpServletRequest request) throws IOException, ParseException {
		String calendarId="";
		String holidayDate="";
		String holidayDescription="";
		String holidayID="";
		if(request.getParameter("location")!=null){
			calendarId=request.getParameter("location");
		}
		if(request.getParameter("datepicker")!=null){
			holidayDate=request.getParameter("datepicker");		
		}
		if(request.getParameter("description")!=null){
			holidayDescription=request.getParameter("description");
		}
		if(request.getParameter("holidayID")!=null){
			holidayID=request.getParameter("holidayID");
		}
		holidayService.updateHolidaysIntoCalendar(holidayID,calendarId, holidayDate, holidayDescription);
		return holidayRegistration(model);
	}
	
	@RequestMapping(value="/deleteHoliday.do",method = RequestMethod.POST)
	public ModelAndView holidayDelete(Model model,
			HttpServletRequest request) throws IOException, ParseException {
		String calendarId="";
		String holidayDate="";
		String holidayDescription="";
		String holidayID="";
		if(request.getParameter("location")!=null){
			calendarId=request.getParameter("location");
		}
		if(request.getParameter("datepicker")!=null){
			holidayDate=request.getParameter("datepicker");		
		}
		if(request.getParameter("description")!=null){
			holidayDescription=request.getParameter("description");
		}
		if(request.getParameter("holidayID")!=null){
			holidayID=request.getParameter("holidayID");
		}
		holidayService.deleteHolidaysIntoCalendar(holidayID,calendarId, holidayDate, holidayDescription);
		return holidayRegistration(model);
	}
	@Autowired
	public void setHolidayService(HolidayService holidayService) {
		this.holidayService = holidayService;
	}
	
}
