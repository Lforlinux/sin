package com.singX.control;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.singX.service.CalendarService;
import com.singX.util.Util;

@Controller

public class CalendarController {
	
	private CalendarService calendarService;
	
	//Calendar admin screen services - start
	
	@RequestMapping(value="/calendarRegistration.do",method = RequestMethod.GET)
	public ModelAndView calculatorRegistration(Model model) throws IOException, ParseException {
		
		Map<String,String>country=new HashMap<String, String>();
		country=Util.getCountries();
		model.addAttribute("locations", country);
		
		List<com.singX.model.Calendar> calendars=calendarService.getAllCalendars();
		model.addAttribute("calendars", calendars);
		
				
		return new ModelAndView("/admin/userRegister");
	}
	
	@RequestMapping(value="/calendarSave.do",method = RequestMethod.POST)
	public ModelAndView calendarSave(Model model,
			HttpServletRequest request) throws IOException, ParseException {
		
		String calendarId="";
		String calendarName="";
		String validityPeriod="";
		String countryCode="";
		String countryName="";
		
		if(request.getParameter("calendarID")!=null){
			calendarId=request.getParameter("calendarID");
		}
		if(request.getParameter("calendarName")!=null){
			calendarName=request.getParameter("calendarName");
		}
		if(request.getParameter("validityPeriod")!=null){
			validityPeriod=request.getParameter("validityPeriod");
		}
		if(request.getParameter("location")!=null){
			countryCode=request.getParameter("location");
		}
		countryName=Util.getCountryName(countryCode);
		
		
		calendarService.calendarSave(calendarId, calendarName, validityPeriod, countryCode, countryName);
		return calculatorRegistration(model);
	}
	
	@RequestMapping(value="/updateCalendar.do",method = RequestMethod.POST)
	public ModelAndView calendarUpdate(Model model,
			HttpServletRequest request) throws IOException, ParseException {
		
		String calendarId="";
		String calendarName="";
		String validityPeriod="";
		String countryCode="";
		String countryName="";
		
		if(request.getParameter("calendarID")!=null){
			calendarId=request.getParameter("calendarID");
		}
		if(request.getParameter("calendarName")!=null){
			calendarName=request.getParameter("calendarName");
		}
		if(request.getParameter("validityPeriod")!=null){
			validityPeriod=request.getParameter("validityPeriod");
		}
		if(request.getParameter("location")!=null){
			countryCode=request.getParameter("location");
		}
		countryName=Util.getCountryName(countryCode);
		
		calendarService.calendarRegistration(calendarId, calendarName, validityPeriod, countryCode, countryName);
		return calculatorRegistration(model);
	}
	
	@RequestMapping(value="/deleteCalendar.do",method = RequestMethod.POST)
	public ModelAndView calendarDelete(Model model,
			HttpServletRequest request) throws IOException, ParseException {
		
		String calendarId="";
		String calendarName="";
		String validityPeriod="";
		String countryCode="";
		String countryName="";
		
		if(request.getParameter("calendarID")!=null){
			calendarId=request.getParameter("calendarID");
		}
		if(request.getParameter("calendarName")!=null){
			calendarName=request.getParameter("calendarName");
		}
		if(request.getParameter("validityPeriod")!=null){
			validityPeriod=request.getParameter("validityPeriod");
		}
		if(request.getParameter("location")!=null){
			countryCode=request.getParameter("location");
		}
		countryName=Util.getCountryName(countryCode);
		
		
		calendarService.calendarDelete(calendarId, calendarName, validityPeriod, countryCode, countryName);
		return calculatorRegistration(model);
	}
	//Calendar admin screen services - end
	
	//Calendar API - start
	@RequestMapping(value="/calendarAPI/calculateValidity.do",method = RequestMethod.POST)
	public @ResponseBody JSONObject calculateValidity(HttpServletResponse response,
			HttpServletRequest request) throws IOException, ParseException {
		String selectedDate="";
		String userId="";
		String calendarId="";
		JSONObject json=new JSONObject();
		
		if((!request.getParameter("calendarId").isEmpty() && request.getParameter("calendarId")!=null)&&
				(!request.getParameter("date").isEmpty() && request.getParameter("date")!=null))
		{	
			selectedDate=request.getParameter("date");
			if(Util.validateDate(selectedDate)){
				if(request.getParameter("uid")!=null){
					userId=request.getParameter("uid");
				}
				if(request.getParameter("calendarId")!=null){
					calendarId=request.getParameter("calendarId");
				}
				
				json=calendarService.calculateValidity(selectedDate,calendarId,userId,request.getParameter("validityPeriod"));
				response.setStatus(200);
			}else{
				json.put("message","date is not valid format");
				response.setStatus(400);
			}
		}else{
			json.put("message","Input data is not received");
			response.setStatus(400);
		}
		return json;
	}
	
	//calculate API in json
	@RequestMapping(value="/calendarAPI/calculateValidityPeriod.do",method = RequestMethod.POST,consumes="application/json")
	public @ResponseBody JSONObject calculateValidityPeriod(HttpServletResponse response,
			HttpServletRequest request,@RequestBody JSONObject requestObj) throws IOException, ParseException {
		String selectedDate="";
		String userId="";
		String calendarId="";
		JSONObject json=new JSONObject();
		if(!requestObj.isEmpty())
		{
			if((requestObj.get("calendarId")!=null &&requestObj.get("calendarId")!="") && (requestObj.get("date")!=null &&requestObj.get("date")!="")){
				selectedDate=requestObj.get("date").toString();
				if(Util.validateDate(selectedDate)){
					if(requestObj.get("calendarId")!=null){
						calendarId=requestObj.get("calendarId").toString();
					}
					
					json=calendarService.calculateValidity(selectedDate,calendarId,userId,requestObj.get("validityPeriod").toString());
					response.setStatus(200);
				}else{
					json.put("message","date is not valid format");
					response.setStatus(400);
				}
			}else{
				json.put("message","date is not valid format");
				response.setStatus(400);
			}
		}else{
			json.put("message","Input data is not received");
			response.setStatus(400);
		}
		System.out.println("Return==>"+json.toString());
		return json;
	}
	//calculate API in json
	
	@RequestMapping(value="/calendarAPI/addDate.do",method = RequestMethod.POST)
	public @ResponseBody JSONObject addDateTime(HttpServletResponse response,
			HttpServletRequest request) throws IOException, ParseException {
		String selectedDate="";
		String userId="";
		String calendarId="";
		String incrementPart="";
		String incrementValue="";
		String gracePeriod="";
		JSONObject json=new JSONObject();
		if((!request.getParameter("calendarId").isEmpty() && request.getParameter("calendarId")!=null)&&
				(!request.getParameter("date").isEmpty() && request.getParameter("date")!=null))
		{	
			selectedDate=request.getParameter("date");
			if(Util.validateDate(selectedDate)){
				if(request.getParameter("uid")!=null){
					userId=request.getParameter("uid");
				}
				if(request.getParameter("calendarId")!=null){
					calendarId=request.getParameter("calendarId");
				}
				if(request.getParameter("incrementPart")!=null){
					incrementPart=request.getParameter("incrementPart");
				}
				if(request.getParameter("incrementValue")!=null){
					incrementValue=request.getParameter("incrementValue");
				}
				if(request.getParameter("gracePeriod")!=null){
					gracePeriod=request.getParameter("gracePeriod");
				}
				json=calendarService.addGracePeriod(selectedDate,calendarId,incrementPart,incrementValue,userId,gracePeriod);
				response.setStatus(200);
			}else{
				json.put("message","date is not valid format");
				response.setStatus(400);
			}
		}else{
			json.put("message","Input data is not received");
			response.setStatus(400);
		}
		return json;
	}
	
	//Calendar API - json
	@RequestMapping(value="/calendarAPI/addDateValidity.do",method = RequestMethod.POST,consumes="application/json")
	public @ResponseBody JSONObject addDateTimeJson(HttpServletResponse response,
			HttpServletRequest request,@RequestBody JSONObject requestObj) throws IOException, ParseException {
		String selectedDate="";
		String userId="";
		String calendarId="";
		String incrementPart="";
		String incrementValue="";
		String gracePeriod="";
		String validityPeriod="";
		JSONObject json=new JSONObject();
		if((requestObj.get("calendarId")!=null && requestObj.get("calendarId")!="")&&
				(requestObj.get("date")!=null && requestObj.get("date")!=""))
		{	
			selectedDate=requestObj.get("date").toString();
			if(Util.validateDate(selectedDate)){
				if(requestObj.get("uid")!=null){
					userId=requestObj.get("uid").toString();
				}
				if(requestObj.get("calendarId")!=null){
					calendarId=requestObj.get("calendarId").toString();
				}
				if(requestObj.get("incrementPart")!=null){
					incrementPart=requestObj.get("incrementPart").toString();
				}
				if(requestObj.get("incrementValue")!=null){
					incrementValue=requestObj.get("incrementValue").toString();
				}
				if(requestObj.get("gracePeriod")!=null){
					gracePeriod=requestObj.get("gracePeriod").toString();
				}
				if(requestObj.get("validityPeriod")!=null){
					validityPeriod=requestObj.get("validityPeriod").toString();
				}
				json=calendarService.addGracePeriod(selectedDate,calendarId,incrementPart,incrementValue,userId,gracePeriod,validityPeriod);
				response.setStatus(200);
			}else{
				json.put("message","date is not valid format");
				response.setStatus(400);
			}
		}else{
			json.put("message","Input data is not received");
			response.setStatus(400);
		}
		return json;
	}
	//Calendar API - json
	@RequestMapping(value="/calendarAPI/slotBooking.do",method = RequestMethod.POST)
	public @ResponseBody JSONObject bookSlot(HttpServletResponse response,
			HttpServletRequest request) throws ParseException  {
		JSONObject json=new JSONObject();
		if((!request.getParameter("customerId").isEmpty() && request.getParameter("customerId")!=null) &&
				(!request.getParameter("verifierId").isEmpty() && request.getParameter("verifierId")!=null) &&
				(!request.getParameter("selectedFromDateTime").isEmpty() && request.getParameter("selectedFromDateTime")!=null) &&
				(!request.getParameter("selectedToDateTime").isEmpty() && request.getParameter("selectedToDateTime")!=null) &&
				(!request.getParameter("calendarId").isEmpty() && request.getParameter("calendarId")!=null)&&
				((!request.getParameter("location").isEmpty() && request.getParameter("location")!=null)))
		{
			if(Util.validateDate(request.getParameter("selectedFromDateTime")) && Util.validateDate(request.getParameter("selectedToDateTime"))){
				
				json=calendarService.bookSlotVerification(request.getParameter("customerId"),
						request.getParameter("verifierId"), 
						request.getParameter("selectedFromDateTime"), 
						request.getParameter("selectedToDateTime"),
						request.getParameter("calendarId"),
						request.getParameter("location"));
				response.setStatus(200);
			}else{
				json.put("message","date is not valid format");
				response.setStatus(400);
			}
		}else{
			json.put("message","Input data is not received");
			response.setStatus(400);
		}
		return json;
	}
	
	@RequestMapping(value="/calendarAPI/getAllSlots.do",method = RequestMethod.POST)
	public @ResponseBody JSONArray getAllSlot(HttpServletResponse response,
			HttpServletRequest request,@RequestBody String value) throws ParseException  {
		JSONArray responseJson=new JSONArray();
		
		if((!request.getParameter("customerId").isEmpty() && request.getParameter("customerId")!=null) &&
				//(!request.getParameter("verifierId").isEmpty() && request.getParameter("verifierId")!=null) &&
				(!request.getParameter("selectedFromDateTime").isEmpty() && request.getParameter("selectedFromDateTime")!=null) &&
				(!request.getParameter("selectedToDateTime").isEmpty() && request.getParameter("selectedToDateTime")!=null) &&
				(!request.getParameter("calendarId").isEmpty() && request.getParameter("calendarId")!=null))
		{
			responseJson=calendarService.getDates("0", request.getParameter("selectedFromDateTime"), request.getParameter("selectedToDateTime"),request.getParameter("calendarId"));
		}
		return responseJson;
	}
	
	@RequestMapping(value="/calendarAPI/checkSlotAvailable.do",method = RequestMethod.POST)
	public @ResponseBody JSONObject checkSlot(HttpServletResponse response,
			HttpServletRequest request) throws ParseException  {
		
		JSONObject json=new JSONObject();
		if((!request.getParameter("customerId").isEmpty() && request.getParameter("customerId")!=null) &&
				//(!request.getParameter("verifierId").isEmpty() && request.getParameter("verifierId")!=null) &&
				(!request.getParameter("selectedFromDateTime").isEmpty() && request.getParameter("selectedFromDateTime")!=null) &&
				(!request.getParameter("selectedToDateTime").isEmpty() && request.getParameter("selectedToDateTime")!=null) &&
				(!request.getParameter("calendarId").isEmpty() && request.getParameter("calendarId")!=null))
		{
			json=calendarService.checkSlots("0", request.getParameter("selectedFromDateTime"), request.getParameter("selectedToDateTime"),request.getParameter("calendarId"));
		}
		return json;
	}
	
	@RequestMapping(value="/calendarAPI/getSlotByFilter.do",method = RequestMethod.POST)
	public @ResponseBody JSONArray getSlotByFilter(HttpServletResponse response,
			HttpServletRequest request) throws ParseException  {
		JSONArray responseJson=new JSONArray();
		JSONObject json=new JSONObject();
		if(((!request.getParameter("selectedFromDateTime").isEmpty() && request.getParameter("selectedFromDateTime")!=null) &&
				(!request.getParameter("selectedToDateTime").isEmpty() && request.getParameter("selectedToDateTime")!=null) &&
				(!request.getParameter("calendarId").isEmpty() && request.getParameter("calendarId")!=null) ))
		{
			if(Util.validateDateOnly(request.getParameter("selectedFromDateTime")) && Util.validateDateOnly(request.getParameter("selectedToDateTime"))){
				responseJson=calendarService.getSlotByVerifier(request.getParameter("selectedFromDateTime"), request.getParameter("selectedToDateTime"),request.getParameter("filterBy"),request.getParameter("customerId"),request.getParameter("verifierId"));
				response.setStatus(200);
			}else{
				json.put("message","date is not valid");
				responseJson.add(json);
				response.setStatus(400);
			}
		}else{
			json.put("message","Input data is not received");
			responseJson.add(json);
			response.setStatus(400);
		}
		return responseJson;
	}
	
	//Calendar API - end
	
	@Autowired
	public void setCalendarService(CalendarService calendarService) {
		this.calendarService = calendarService;
	}
	
}
