package com.singX.dao;

import java.util.List;

import com.singX.model.Calendar;
import com.singX.model.HolidayList;

public interface CalendarDAO {

	 List<HolidayList> getHolidayList(String calendarId);
	 
	 List<Calendar> getCalendars(String calendarId);
	 
	 List<Calendar> getAllCalendars();
	 
	 void saveCalendar(Calendar calendar);
	 
	 void save(Calendar calendar);
}
