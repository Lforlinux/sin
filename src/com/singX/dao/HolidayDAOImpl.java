package com.singX.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.singX.model.HolidayList;

@Repository
public class HolidayDAOImpl implements HolidayDAO{
	
private HibernateTemplate hibernateTemplate;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	public void saveHolidays(HolidayList holiday) {
		// TODO Auto-generated method stub
		hibernateTemplate.saveOrUpdate(holiday);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<HolidayList> getAllHoliday(){
		return hibernateTemplate.find("from HolidayList where isValidRec=1");
	}

}
