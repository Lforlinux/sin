package com.singX.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.singX.model.Calendar;
import com.singX.model.HolidayList;

@Repository
public class CalendarDAOImpl implements CalendarDAO {

	private HibernateTemplate hibernateTemplate;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<HolidayList> getHolidayList(String calendarId){
		return hibernateTemplate.find(" from HolidayList where calendarId = ? and isValidRec=1",calendarId);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Calendar> getCalendars(String calendarId){
		return hibernateTemplate.find("from Calendar where calendarId = ? and isValidRec=1",calendarId);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Calendar> getAllCalendars(){
		return hibernateTemplate.find("from Calendar where isValidRec=1");
	}
	
	@Override
	public void saveCalendar(Calendar calendar){
		hibernateTemplate.saveOrUpdate(calendar);
	}
	
	@Override
	public void save(Calendar calendar){
		hibernateTemplate.save(calendar);
	}
}
