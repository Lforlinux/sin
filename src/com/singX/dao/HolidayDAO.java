package com.singX.dao;

import java.util.List;

import com.singX.model.HolidayList;

public interface HolidayDAO {

	void saveHolidays(HolidayList holiday);
	
	List<HolidayList> getAllHoliday();
}
