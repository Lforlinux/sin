package com.singX.dao;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.singX.model.TimeSlotsAvailability;
import com.singX.model.VerifcationDetails;
import com.singX.model.VerifierDetails;


@Repository
public class VerificationDetailsDAOImpl implements VerificationDetailsDAO {

private HibernateTemplate hibernateTemplate;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	@Override
	public List<VerifcationDetails> save(VerifcationDetails details) {
		// TODO Auto-generated method stub
		hibernateTemplate.saveOrUpdate(details);
		List<VerifcationDetails> slotDetails=getVerifcationDetails(details.getVerifierId(),details.getCustomerId(),details.getScheduledFrom(),details.getScheduledTo(),details.getLocation());
		return slotDetails;
	}
	
	@Override
	public void update(VerifcationDetails details) {
		hibernateTemplate.update(details);
	}
	
	@Override
	public void saveVerifier(VerifierDetails verififerDetails) {
		// TODO Auto-generated method stub
		hibernateTemplate.saveOrUpdate(verififerDetails);
	}
	
	@Override
	public void saveAvailability(TimeSlotsAvailability availability) {
		// TODO Auto-generated method stub
		hibernateTemplate.saveOrUpdate(availability);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<VerifierDetails> getAllVerifiers() {
		// TODO Auto-generated method stub
		return hibernateTemplate.find("from VerifierDetails where isValidRec=1");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VerifcationDetails> getVerifcationDetails(String verifierId,String date) {
		// TODO Auto-generated method stub
		return hibernateTemplate.find("from VerifcationDetails where verifierId=? and isValidRec=1 and DATE_FORMAT(scheduledFrom,'%Y-%m-%d')=? and isValidRec=1 order by verifierId,scheduledFrom",verifierId,date);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<VerifcationDetails> getVerifcationDetails(String verifierId,String fromDate,String toDate) {
		// TODO Auto-generated method stub
		return hibernateTemplate.find("from VerifcationDetails where verifierId=? and isValidRec=1 and DATE_FORMAT(scheduledFrom,'%Y-%m-%d') BETWEEN ? AND ? order by scheduledFrom",verifierId,fromDate,toDate);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<VerifcationDetails> getCustomerVerifcationDetails(String customerId,String date) {
		// TODO Auto-generated method stub
		return hibernateTemplate.find("from VerifcationDetails where customerId=? and isValidRec=1 and DATE_FORMAT(scheduledFrom,'%Y-%m-%d')=? and isValidRec=1",customerId,date);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<VerifcationDetails> getCustomerVerifcationDetails(String customerId,String fromDate,String toDate) {
		// TODO Auto-generated method stub
		return hibernateTemplate.find("from VerifcationDetails where customerId=? and DATE_FORMAT(scheduledFrom,'%Y-%m-%d') BETWEEN ? AND ?  and isValidRec=1 order by scheduledFrom",customerId,fromDate,toDate);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<VerifcationDetails> getVerifcationDetails(String verifierId,String customerId,Timestamp fromDate,Timestamp toDate) {
		// TODO Auto-generated method stub
		return hibernateTemplate.find("from VerifcationDetails where verifierId=? and customerId=? and scheduledFrom=? and scheduledTo=? and isValidRec=1",verifierId,customerId,fromDate,toDate);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<VerifcationDetails> getVerifcationDetails(String verifierId,String customerId,Timestamp fromDate,Timestamp toDate,String location) {
		// TODO Auto-generated method stub
		return hibernateTemplate.find("from VerifcationDetails where verifierId=? and customerId=? and scheduledFrom=? and scheduledTo=? and isValidRec=1",verifierId,customerId,fromDate,toDate);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TimeSlotsAvailability> getAvailabilityByVerifier(String verifierId,String fromDate,String toDate) {
		//return hibernateTemplate.find("from TimeSlotsAvailability where verifierId=? and isValidRec=1 and DATE_FORMAT(availableFrom,'%Y-%m-%d')=?",verifierId,date);
		return hibernateTemplate.find("from TimeSlotsAvailability WHERE  DATE_FORMAT(availableFrom,'%Y-%m-%d') BETWEEN ? AND ? and isValidRec=1 order by verifierId,availableFrom",fromDate,toDate);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TimeSlotsAvailability> getAvailabilityByVerifier(String verifierId,String fromDate,String toDate,String calendarId) {
		//return hibernateTemplate.find("from TimeSlotsAvailability where verifierId=? and isValidRec=1 and DATE_FORMAT(availableFrom,'%Y-%m-%d')=?",verifierId,date);
		return hibernateTemplate.find("from TimeSlotsAvailability WHERE  DATE_FORMAT(availableFrom,'%Y-%m-%d') BETWEEN ? AND ? and calendarId=? and isValidRec=1 order by verifierId,availableFrom",fromDate,toDate,calendarId);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TimeSlotsAvailability> getAvailabilitySlots(String verifierId,String date) {
		//return hibernateTemplate.find("from TimeSlotsAvailability where verifierId=? and isValidRec=1 and DATE_FORMAT(availableFrom,'%Y-%m-%d')=?",verifierId,date);
		return hibernateTemplate.find("from TimeSlotsAvailability WHERE  DATE_FORMAT(availableFrom,'%Y-%m-%d')=? and verifierId=? order by verifierId,availableFrom",date,verifierId);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TimeSlotsAvailability> getAllAvailabilitySlotByVerifier(String verifierId,String date,String calendarId) {
		//return hibernateTemplate.find("from TimeSlotsAvailability where verifierId=? and isValidRec=1 and DATE_FORMAT(availableFrom,'%Y-%m-%d')=?",verifierId,date);
		return hibernateTemplate.find("from TimeSlotsAvailability WHERE  DATE_FORMAT(availableFrom,'%Y-%m-%d')=? and verifierId=? and calendarId=? order by verifierId,availableFrom",date,verifierId,calendarId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TimeSlotsAvailability> getAvailabilityByVerifier(
			String verifierId, java.util.Date fromDate, java.util.Date toDate) {
		// TODO Auto-generated method stub
		//return hibernateTemplate.find("from TimeSlotsAvailability WHERE  availableFrom BETWEEN ? AND ? and isValidRec=1 order by verifierId,availableFrom",fromDate,toDate);
		return hibernateTemplate.find("from TimeSlotsAvailability WHERE DATE_FORMAT(availableFrom,'%Y-%m-%d') >=? and DATE_FORMAT(availableTo,'%Y-%m-%d') <=? order by verifierId,availableFrom",fromDate,toDate);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TimeSlotsAvailability> getAvailabilityByVerifierId(String verifierId,String fromDate,String toDate) {
		//return hibernateTemplate.find("from TimeSlotsAvailability where verifierId=? and isValidRec=1 and DATE_FORMAT(availableFrom,'%Y-%m-%d')=?",verifierId,date);
		return hibernateTemplate.find("from TimeSlotsAvailability WHERE verifierId=? and DATE_FORMAT(availableFrom,'%Y-%m-%d') BETWEEN ? AND ? and isValidRec=1 order by verifierId,availableFrom",verifierId,fromDate,toDate);
	}
	
	/*@SuppressWarnings("unchecked")
	@Override
	public List<TimeSlotsAvailability> getAvailabilityByVerifier(
			Integer verifierId, String fromDate, java.util.Date toDate) {
		// TODO Auto-generated method stub
		//return hibernateTemplate.find("from TimeSlotsAvailability WHERE  availableFrom BETWEEN ? AND ? and isValidRec=1 order by verifierId,availableFrom",fromDate,toDate);
		return hibernateTemplate.find("from TimeSlotsAvailability WHERE availableFrom >=? and availableTo <=? order by verifierId,availableFrom",fromDate,toDate);
	}*/
	
	@SuppressWarnings("unchecked")
	@Override
	public List<VerifcationDetails> getAllScheduledCustomers(String fromDate,String toDate,String customerId)
	{
		return hibernateTemplate.find("from VerifcationDetails where DATE_FORMAT(scheduledFrom,'%Y-%m-%d') BETWEEN ? AND ? and customerId=? and isValidRec=1 order by scheduledFrom",fromDate,toDate,customerId);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<VerifcationDetails> getAllScheduledVerifiers(String fromDate,String toDate,String verifierId)
	{
		return hibernateTemplate.find("from VerifcationDetails where DATE_FORMAT(scheduledFrom,'%Y-%m-%d') BETWEEN ? AND ? and verifierId=? and isValidRec=1 order by scheduledFrom",fromDate,toDate,verifierId);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<VerifcationDetails> getAllScheduled(String fromDate,String toDate)
	{
		return hibernateTemplate.find("from VerifcationDetails where DATE_FORMAT(scheduledFrom,'%Y-%m-%d') BETWEEN ? AND ? and isValidRec=1 order by scheduledFrom",fromDate,toDate);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<VerifcationDetails> getAllVerificationList()
	{
		return hibernateTemplate.find("from VerifcationDetails where isValidRec=1 order by verifierId,scheduledFrom");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TimeSlotsAvailability> getAllVerifierAvailability() {
		//return hibernateTemplate.find("from TimeSlotsAvailability where verifierId=? and isValidRec=1 and DATE_FORMAT(availableFrom,'%Y-%m-%d')=?",verifierId,date);
		return hibernateTemplate.find("from TimeSlotsAvailability WHERE isValidRec=1 order by verifierId,availableFrom");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TimeSlotsAvailability> getAllVerifierAvailabilityFromNow() {
		//return hibernateTemplate.find("from TimeSlotsAvailability where verifierId=? and isValidRec=1 and DATE_FORMAT(availableFrom,'%Y-%m-%d')=?",verifierId,date);
		return hibernateTemplate.find("from TimeSlotsAvailability WHERE isValidRec=1 and DATE_FORMAT(availableFrom,'%Y-%m-%d')>=CURDATE() order by verifierId,availableFrom");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TimeSlotsAvailability> getAllAvailability() {
		return hibernateTemplate.find("select availabilityId,verifierId,calendarId,availableFrom,availableTo,isValidRec,DATE_FORMAT(availableFrom,'%H') as fromHour,DATE_FORMAT(availableFrom,'%i') as fromMinute,DATE_FORMAT(availableTo,'%H') as toHour,"+
				"DATE_FORMAT(availableTo,'%i') as toMinute from TimeSlotsAvailability WHERE isValidRec=1 order by verifierId,availableFrom");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VerifcationDetails> getAllSlots(String fromDate,String calendarId)
	{
		return hibernateTemplate.find("from VerifcationDetails where DATE_FORMAT(scheduledFrom,'%Y-%m')=? and calendarId=? and isValidRec=1 order by scheduledFrom",fromDate,calendarId);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getAllBookedSlots()
	{
		//return hibernateTemplate.find("from VerifcationDetails as comp left outer join comp.Singxcontacts as emp");
		return hibernateTemplate.find("select a.customerId,b.firstName,b.lastName from CustomerXContacts a,Singxcontacts b WHERE a.contactId = b.contactId ");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<VerifcationDetails> getAllSlotByVerifier(String fromDate,String calendarId,String verifierId,Integer scheduledId)
	{
		return hibernateTemplate.find("from VerifcationDetails where DATE_FORMAT(scheduledFrom,'%Y-%m-%d')=? and calendarId=? and verifierId=? and scheduledId!=? and isValidRec=1 order by scheduledFrom",fromDate,calendarId,verifierId,scheduledId);
	}
}