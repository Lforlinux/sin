package com.singX.dao;

import java.sql.Timestamp;
import java.util.List;

import com.singX.model.TimeSlotsAvailability;
import com.singX.model.VerifcationDetails;
import com.singX.model.VerifierDetails;

public interface VerificationDetailsDAO {
	
	List<VerifcationDetails> save(VerifcationDetails details);
	
	void saveVerifier(VerifierDetails verififerDetails);
	
	 void update(VerifcationDetails details);
	
	List<VerifierDetails> getAllVerifiers();
	
	void saveAvailability(TimeSlotsAvailability availability);

	List<VerifcationDetails> getVerifcationDetails(String verifierId,String date);
	
	List<VerifcationDetails> getVerifcationDetails(String verifierId,String fromDate,String toDate);
	
	List<VerifcationDetails> getCustomerVerifcationDetails(String customerId,String date);
	
	List<VerifcationDetails> getCustomerVerifcationDetails(String customerId,String fromDate,String toDate);
	
	List<VerifcationDetails> getVerifcationDetails(String verifierId,String customerId,Timestamp fromDate,Timestamp toDate);
	
	List<VerifcationDetails> getVerifcationDetails(String verifierId,String customerId,Timestamp fromDate,Timestamp toDate,String location);
	
	List<TimeSlotsAvailability> getAvailabilityByVerifier(String verifierId,String fromDate,String toDate);
	
	List<TimeSlotsAvailability> getAvailabilityByVerifier(String verifierId,String fromDate,String toDate,String calendarId);
	
	List<TimeSlotsAvailability> getAvailabilityByVerifierId(String verifierId,String fromDate,String toDate);
	
	List<TimeSlotsAvailability> getAvailabilityByVerifier(String verifierId,java.util.Date fromDate,java.util.Date toDate);
	
	List<TimeSlotsAvailability> getAvailabilitySlots(String verifierId,String date);
	
	List<VerifcationDetails> getAllScheduledVerifiers(String fromDate,String toDate,String verifierId);
	
	List<VerifcationDetails> getAllScheduledCustomers(String fromDate,String toDate,String customerId);
	
	List<VerifcationDetails> getAllScheduled(String fromDate,String toDate);
	
	List<VerifcationDetails> getAllVerificationList();
	
	List<TimeSlotsAvailability> getAllVerifierAvailability();
	
	List<TimeSlotsAvailability> getAllVerifierAvailabilityFromNow();
	
	List<TimeSlotsAvailability> getAllAvailability() ;
	
	List<VerifcationDetails> getAllSlots(String fromDate,String calendarId);
	
	List<Object[]> getAllBookedSlots();
	
	List<TimeSlotsAvailability> getAllAvailabilitySlotByVerifier(String verifierId,String date,String calendarId);
	
	List<VerifcationDetails> getAllSlotByVerifier(String fromDate,String calendarId,String verifierId,Integer scheduledId);
	
}
