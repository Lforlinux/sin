package com.singX.constants;

public final class LabelConstants {
	
	public static final String employeeId="Please enter the employee ID";
	public static final String firstName="Please enter the firstname";
	public static final String lastName="Please enter the lastname";
	public static final String userName="Please enter the username";
	public static final String password="Please enter the password";
	public static final String invalidEmail="Please enter a valid email Id";
	public static final String email="Please enter a email Id";
	public static final String location="Please select the location";
	public static final String businessUnit="Please select the business unit";
	public static final String subBusinessUnit="Please select the sub business unit";
	public static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public static final String timeFormat="yyyy-MM-dd HH:mm:ss";
	public static final String dateFormat="yyyy-MM-dd";

}
