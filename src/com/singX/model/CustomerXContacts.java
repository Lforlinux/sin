package com.singX.model;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ast_CustomerXContacts_T")
public class CustomerXContacts {
	
	@Id
	@Column(name="mapPk",nullable=false)
	private String 	mapPk;
	
	@Column(name="contactId")
	private String contactId;
	
	@Column(name="customerId")
	private String customerId;
	
	@Column(name="createdBy")
	private String createdBy;
	
	@Column(name="createdDate")
	private Date createdDate;
	
	@Column(name="updatedBy")
	private String updatedBy;
	
	@Column(name="updatedDate")
	private Date updatedDate;

	@Column(name="versionId")
	private int versionId;
	
	@Column(name="activeStatus")
	private int activeStatus;
	
	@Column(name="txnAccessCode")
	private int txnAccessCode;

	public String getMapPk() {
		return mapPk;
	}

	public void setMapPk(String mapPk) {
		this.mapPk = mapPk;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public int getVersionId() {
		return versionId;
	}

	public void setVersionId(int versionId) {
		this.versionId = versionId;
	}

	public int getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}

	public int getTxnAccessCode() {
		return txnAccessCode;
	}

	public void setTxnAccessCode(int txnAccessCode) {
		this.txnAccessCode = txnAccessCode;
	}
	

	
	
}
