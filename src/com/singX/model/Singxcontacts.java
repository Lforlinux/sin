package com.singX.model;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ast_SingxContacts_T")
public class Singxcontacts {
	
	@Id
	@Column(name="contactId",nullable=false)
	private String contactId;
	
	@Column(name="prefixId",nullable=false)
	private String prefixId;
	
	@Column(name="firstName",nullable=false)
	private String firstName;
	
	@Column(name="middleName",nullable=false)
	private String middleName;
	
	@Column(name="lastName",nullable=false)
	private String lastName;
	
	@Column(name="genderId",nullable=false)
	private String genderId;
	
	
	@Column(name="dateOfBirth",nullable=false)
	private Date dateOfBirth;
	
	@Column(name="emailId",nullable=false)
	private String emailId;
	
	
	@Column(name="phoneCountryCode",nullable=false)
	private String phoneCountryCode;
	
	
	@Column(name="phoneNumber",nullable=false)
	private String phoneNumber;
	
	@Column(name="countryId",nullable=false)
	private String countryId;
	
	@Column(name="docStatusId",nullable=false)
	private int docStatusId;
	
	
	@Column(name="verStatusId",nullable=false)
	private int verStatusId;
	
	
	@Column(name="kycStatusId",nullable=false)
	private int kycStatusId;
	
	@Column(name="createdBy",nullable=false)
	private String createdBy;
	
	@Column(name="createdDate",nullable=false)
	private Timestamp createdDate;
	
	
	@Column(name="updatedBy",nullable=false)
	private String updatedBy;
	
	
	@Column(name="updatedDate",nullable=false)
	private Date updatedDate; 
	
	
	@Column(name="versionId",nullable=false)
	private int versionId;
	
	@Column(name="activeStatus",nullable=false)
	private int activeStatus;
	
	@Column(name="txnAccessCode",nullable=false)
	private int txnAccessCode;
	
	@Column(name="occupation",nullable=false)
	private String occupation;
	
	@Column(name="otherOccupation",nullable=false)
	private String otherOccupation;
	
	@Column(name="termCondition",nullable=false)
	private String termCondition;
	
	@Column(name="marketComm",nullable=false)
	private String marketComm;
	

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public String getPrefixId() {
		return prefixId;
	}

	public void setPrefixId(String prefixId) {
		this.prefixId = prefixId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGenderId() {
		return genderId;
	}

	public void setGenderId(String genderId) {
		this.genderId = genderId;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneCountryCode() {
		return phoneCountryCode;
	}

	public void setPhoneCountryCode(String phoneCountryCode) {
		this.phoneCountryCode = phoneCountryCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public int getDocStatusId() {
		return docStatusId;
	}

	public void setDocStatusId(int docStatusId) {
		this.docStatusId = docStatusId;
	}

	public int getVerStatusId() {
		return verStatusId;
	}

	public void setVerStatusId(int verStatusId) {
		this.verStatusId = verStatusId;
	}

	public int getKycStatusId() {
		return kycStatusId;
	}

	public void setKycStatusId(int kycStatusId) {
		this.kycStatusId = kycStatusId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public int getVersionId() {
		return versionId;
	}

	public void setVersionId(int versionId) {
		this.versionId = versionId;
	}

	public int getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}

	public int getTxnAccessCode() {
		return txnAccessCode;
	}

	public void setTxnAccessCode(int txnAccessCode) {
		this.txnAccessCode = txnAccessCode;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getOtherOccupation() {
		return otherOccupation;
	}

	public void setOtherOccupation(String otherOccupation) {
		this.otherOccupation = otherOccupation;
	}

	public String getTermCondition() {
		return termCondition;
	}

	public void setTermCondition(String termCondition) {
		this.termCondition = termCondition;
	}

	public String getMarketComm() {
		return marketComm;
	}

	public void setMarketComm(String marketComm) {
		this.marketComm = marketComm;
	}
	
}
