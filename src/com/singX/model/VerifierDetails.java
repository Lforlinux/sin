package com.singX.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="verifier_details")
public class VerifierDetails {

	@Id
	@Column(name="verifier_id",nullable=false)
	private String verifierId;
	
	@Column(name="verifier_name")
	private String verifierName;
	
	@Column(name="country_code")
	private String countryCode;
	
	@Column(name="is_valid_rec")
	private Integer isValidRec;

	public String getVerifierId() {
		return verifierId;
	}

	public void setVerifierId(String verifierId) {
		this.verifierId = verifierId;
	}

	public String getVerifierName() {
		return verifierName;
	}

	public void setVerifierName(String verifierName) {
		this.verifierName = verifierName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Integer getIsValidRec() {
		return isValidRec;
	}

	public void setIsValidRec(Integer isValidRec) {
		this.isValidRec = isValidRec;
	}
	
	
}
