package com.singX.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="calendars")
public class Calendar {

	@Id
	@Column(name="calendar_id",nullable=false)
	private String calendarId;
	
	@Column(name="calendar_name")
	private String calendarName;
	
	@Column(name="country_name")
	private String countryName;
	
	@Column(name="country_code")
	private String countryCode;

	@Column(name="validity_period")
	private Integer validityPeriod;
	
	/*@Column(name="weekends")
	private String weekends;*/
	
	@Column(name="is_valid_rec")
	private Integer isValidRec;

	public String getCalendarId() {
		return calendarId;
	}

	public void setCalendarId(String calendarId) {
		this.calendarId = calendarId;
	}

	public String getCalendarName() {
		return calendarName;
	}

	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}
	
	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Integer getValidityPeriod() {
		return validityPeriod;
	}

	public void setValidityPeriod(Integer validityPeriod) {
		this.validityPeriod = validityPeriod;
	}
	
	/*public String getWeekends() {
		return weekends;
	}

	public void setWeekends(String weekends) {
		this.weekends = weekends;
	}*/

	public Integer getIsValidRec() {
		return isValidRec;
	}

	public void setIsValidRec(Integer isValidRec) {
		this.isValidRec = isValidRec;
	}
	
	
}
