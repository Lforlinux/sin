package com.singX.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="verifcation_details")
public class VerifcationDetails {
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")  
	@Column(name = "scheduled_id" )
	private Integer scheduledId;
	
	@Column(name="verifier_id")
	private String verifierId;
	
	@Column(name="customer_id")
	private String customerId;
	
	@Column(name="calendar_id")
	private String calendarId;
	
	@Column(name="scheduled_from")
	private Timestamp scheduledFrom;
	
	@Column(name="scheduled_to")
	private Timestamp scheduledTo;
	
	@Column(name="is_valid_rec")
	private Integer isValidRec;
	
	private String customerName;
	
	@Column(name="location")
	private String location;

	public Integer getScheduledId() {
		return scheduledId;
	}

	public void setScheduledId(Integer scheduledId) {
		this.scheduledId = scheduledId;
	}

	public String getVerifierId() {
		return verifierId;
	}

	public void setVerifierId(String verifierId) {
		this.verifierId = verifierId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Timestamp getScheduledFrom() {
		return scheduledFrom;
	}

	public void setScheduledFrom(Timestamp scheduledFrom) {
		this.scheduledFrom = scheduledFrom;
	}

	public Timestamp getScheduledTo() {
		return scheduledTo;
	}

	public void setScheduledTo(Timestamp scheduledTo) {
		this.scheduledTo = scheduledTo;
	}

	public Integer getIsValidRec() {
		return isValidRec;
	}

	public void setIsValidRec(Integer isValidRec) {
		this.isValidRec = isValidRec;
	}

	public String getCalendarId() {
		return calendarId;
	}

	public void setCalendarId(String calendarId) {
		this.calendarId = calendarId;
	}

	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	
	
	
	
	/*@GenericGenerator(name="kaugen" , strategy="increment")
	@GeneratedValue(generator="kaugen")
	@Column(name="scheduled_id")
	  public Integer getMaxId() {
	    return scheduledId;
	 }*/
	
}
