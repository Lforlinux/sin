package com.singX.model;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="time_slots_availability")
public class TimeSlotsAvailability {

	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")  
	@Column(name="availability_id")
	private Integer availabilityId;
	
	@Column(name="verifier_id")
	private String verifierId;
	
	@Column(name="calendar_id")
	private String calendarId;
	
	@Column(name="available_from")
	private Timestamp availableFrom;
	
	@Column(name="available_to")
	private Timestamp availableTo;
	
	@Column(name="is_valid_rec")
	private Integer isValidRec;

	public Integer getAvailabilityId() {
		return availabilityId;
	}

	public void setAvailabilityId(Integer availabilityId) {
		this.availabilityId = availabilityId;
	}

	public String getVerifierId() {
		return verifierId;
	}

	public void setVerifierId(String verifierId) {
		this.verifierId = verifierId;
	}

	public String getCalendarId() {
		return calendarId;
	}

	public void setCalendarId(String calendarId) {
		this.calendarId = calendarId;
	}

	public Timestamp getAvailableFrom() {
		return availableFrom;
	}

	public void setAvailableFrom(Timestamp availableFrom) {
		this.availableFrom = availableFrom;
	}

	public Timestamp getAvailableTo() {
		return availableTo;
	}

	public void setAvailableTo(Timestamp availableTo) {
		this.availableTo = availableTo;
	}

	public Integer getIsValidRec() {
		return isValidRec;
	}

	public void setIsValidRec(Integer isValidRec) {
		this.isValidRec = isValidRec;
	}

	
}
