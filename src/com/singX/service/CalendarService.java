package com.singX.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singX.constants.LabelConstants;
import com.singX.dao.CalendarDAO;
import com.singX.dao.VerificationDetailsDAO;
import com.singX.model.HolidayList;
import com.singX.model.TimeSlotsAvailability;
import com.singX.model.VerifcationDetails;
import com.singX.util.Util;

@Service
public class CalendarService {

	@Autowired
	public CalendarDAO calendarDAO;
	@Autowired
	public VerificationDetailsDAO verificationDetailsDAO;
	public JSONObject obj;
	
	//Calendar API - start
	public JSONObject calculateValidity(String selectedDate,String calendarId,String userId,String validityPeriod) throws ParseException{
		obj=new JSONObject();
		obj.clear();
	   // String newFormat = "yyyy-MM-dd HH:mm:ss";
	    SimpleDateFormat sdf2 = new SimpleDateFormat(LabelConstants.timeFormat);
	    Date selDate=sdf2.parse(selectedDate);
	    Date calculateDate =new Date();
	    obj.put("uid", userId);
	    obj.put("selectedDate",selectedDate);
	    obj.put("isHoliday","false");
	    Calendar cal = Calendar.getInstance(); 
	    cal.setTime(selDate); 
	    if(validityPeriod!=null && !validityPeriod.equalsIgnoreCase("")){
	    	cal.add(Calendar.HOUR,Integer.parseInt(validityPeriod));
	    }else{
	    	cal.add(Calendar.HOUR,getValidityPeriod(calendarId));
	    }
	    obj.put("actualDate",new SimpleDateFormat(LabelConstants.timeFormat).format(cal.getTime()));
	    
	    //get list of public holidays for the selected calendar - starts
	    List<HolidayList> holidays=new ArrayList<HolidayList>();
	    List<String>holidayList=new ArrayList<String>();
	    holidays=calendarDAO.getHolidayList(calendarId);
	    for(HolidayList holiday:holidays){
	    	holidayList.add(new SimpleDateFormat(LabelConstants.dateFormat).format(holiday.getHolidayDate().getTime()));
	    }
	    //get list of public holidays for the selected calendar - ends
	    
	    //validating the calculated date is public holiday - start
	    Calendar finalDate=checkHoliday(holidayList, cal);
	    calculateDate = finalDate.getTime();   
	    obj.put("calculatedDate", new SimpleDateFormat(LabelConstants.timeFormat).format(calculateDate.getTime()));
	    //validating the calculated date is public holiday - End	    
	    return obj;
	}
	
	public Calendar checkHoliday(List<String>holidayList,Calendar date){
		if(holidayList.contains(new SimpleDateFormat(LabelConstants.dateFormat).format(date.getTime()))){
			obj.put("isHoliday","true");
			date.add(Calendar.DATE, 1);
		}
		if(holidayList.contains(new SimpleDateFormat(LabelConstants.dateFormat).format(date.getTime()))){
			return checkHoliday(holidayList, date);
		}else{
			return date;
		}
	}
	
	public JSONObject addGracePeriod(String selectedDate,String calendarId,String dateORtime,String incrementValue,String userId,String gracePeriod) throws ParseException{
		obj=new JSONObject();
		obj.clear();
	   // String newFormat = "yyyy-MM-dd HH:mm:ss";
	    SimpleDateFormat sdf2 = new SimpleDateFormat(LabelConstants.timeFormat);
	    Date selDate=sdf2.parse(selectedDate);
	    Date calculateDate =new Date();
	    obj.put("uid", userId);
	    obj.put("selectedDate",selectedDate);
	    obj.put("isHoliday","false");
	    Calendar cal = Calendar.getInstance(); 
	    cal.setTime(selDate); 
	   // cal.add(Calendar.HOUR,getValidityPeriod(calendarId));
	    //Bug fix
	    //Adding grace period and calculating the date - start
	    if(dateORtime.equalsIgnoreCase("yyyy")){
	    	cal.add(Calendar.YEAR,Integer.parseInt(incrementValue));
	    	if(gracePeriod!=null && !gracePeriod.equalsIgnoreCase("")){
	    		cal.add(Calendar.YEAR,Integer.parseInt(gracePeriod));
	    	}
		}else if(dateORtime.equalsIgnoreCase("mm")){
			cal.add(Calendar.MONTH,Integer.parseInt(incrementValue));
	    	if(gracePeriod!=null && !gracePeriod.equalsIgnoreCase("")){
	    		cal.add(Calendar.MONTH,Integer.parseInt(gracePeriod));
	    	}
		}else if(dateORtime.equalsIgnoreCase("dd")){
			cal.add(Calendar.DATE,Integer.parseInt(incrementValue));
	    	if(gracePeriod!=null && !gracePeriod.equalsIgnoreCase("")){
	    		cal.add(Calendar.DATE,Integer.parseInt(gracePeriod));
	    	}
		}else if(dateORtime.equalsIgnoreCase("hh")){
			cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(incrementValue));
			if(gracePeriod!=null && !gracePeriod.equalsIgnoreCase("")){
				cal.add(Calendar.HOUR_OF_DAY,Integer.parseInt(gracePeriod));
	    	}
		}else if(dateORtime.equalsIgnoreCase("ms")){
			cal.add(Calendar.MINUTE, Integer.parseInt(incrementValue));
			if(gracePeriod!=null && !gracePeriod.equalsIgnoreCase("")){
				cal.add(Calendar.MINUTE,Integer.parseInt(gracePeriod));
	    	}
		}
	    //Adding grace period and calculating the date - end
	    //Bug fix
	    obj.put("actualDate",new SimpleDateFormat(LabelConstants.timeFormat).format(cal.getTime()));
	    
	    //get list of public holidays for the selected calendar - start
	    List<HolidayList> holidays=new ArrayList<HolidayList>();
	    List<String>holidayList=new ArrayList<String>();
	    holidays=calendarDAO.getHolidayList(calendarId);
	    for(HolidayList holiday:holidays){
	    	holidayList.add(new SimpleDateFormat(LabelConstants.dateFormat).format(holiday.getHolidayDate().getTime()));
	    }
	    //get list of public holidays for the selected calendar - ends
	    
	    //validating the calculated date is public holiday - start
	    Calendar finalDate=checkHoliday(holidayList, cal);
	    calculateDate = finalDate.getTime();
	    //validating the calculated date is public holiday - End	
	    
	    finalDate=checkHoliday(holidayList, cal);
	    calculateDate = finalDate.getTime();
	    
	    obj.put("calculatedDate", new SimpleDateFormat(LabelConstants.timeFormat).format(calculateDate.getTime()));
	        
	    return obj;
	}
	
	public JSONObject addGracePeriod(String selectedDate,String calendarId,String dateORtime,String incrementValue,String userId,String gracePeriod,String validityPeriod) throws ParseException{
		obj=new JSONObject();
		obj.clear();
	   // String newFormat = "yyyy-MM-dd HH:mm:ss";
	    SimpleDateFormat sdf2 = new SimpleDateFormat(LabelConstants.timeFormat);
	    Date selDate=sdf2.parse(selectedDate);
	    Date calculateDate =new Date();
	    obj.put("uid", userId);
	    obj.put("selectedDate",selectedDate);
	    obj.put("isHoliday","false");
	    Calendar cal = Calendar.getInstance(); 
	    cal.setTime(selDate); 
	    //
	    if(validityPeriod!=null && validityPeriod!=""){
	    	cal.add(Calendar.HOUR,Integer.parseInt(validityPeriod));
	    }else{
	    	cal.add(Calendar.HOUR,getValidityPeriod(calendarId));
	    }
	    //
	    
	    //Bug fix
	    //Adding grace period and calculating the date - start
	    if(dateORtime.equalsIgnoreCase("yyyy")){
	    	cal.add(Calendar.YEAR,Integer.parseInt(incrementValue));
	    	if(gracePeriod!=null && !gracePeriod.equalsIgnoreCase("")){
	    		cal.add(Calendar.YEAR,Integer.parseInt(gracePeriod));
	    	}
		}else if(dateORtime.equalsIgnoreCase("mm")){
			cal.add(Calendar.MONTH,Integer.parseInt(incrementValue));
	    	if(gracePeriod!=null && !gracePeriod.equalsIgnoreCase("")){
	    		cal.add(Calendar.MONTH,Integer.parseInt(gracePeriod));
	    	}
		}else if(dateORtime.equalsIgnoreCase("dd")){
			cal.add(Calendar.DATE,Integer.parseInt(incrementValue));
	    	if(gracePeriod!=null && !gracePeriod.equalsIgnoreCase("")){
	    		cal.add(Calendar.DATE,Integer.parseInt(gracePeriod));
	    	}
		}else if(dateORtime.equalsIgnoreCase("hh")){
			cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(incrementValue));
			if(gracePeriod!=null && !gracePeriod.equalsIgnoreCase("")){
				cal.add(Calendar.HOUR_OF_DAY,Integer.parseInt(gracePeriod));
	    	}
		}else if(dateORtime.equalsIgnoreCase("ms")){
			cal.add(Calendar.MINUTE, Integer.parseInt(incrementValue));
			if(gracePeriod!=null && !gracePeriod.equalsIgnoreCase("")){
				cal.add(Calendar.MINUTE,Integer.parseInt(gracePeriod));
	    	}
		}
	    //Adding grace period and calculating the date - end
	    //Bug fix
	    obj.put("actualDate",new SimpleDateFormat(LabelConstants.timeFormat).format(cal.getTime()));
	    
	    //get list of public holidays for the selected calendar - start
	    List<HolidayList> holidays=new ArrayList<HolidayList>();
	    List<String>holidayList=new ArrayList<String>();
	    holidays=calendarDAO.getHolidayList(calendarId);
	    for(HolidayList holiday:holidays){
	    	holidayList.add(new SimpleDateFormat(LabelConstants.dateFormat).format(holiday.getHolidayDate().getTime()));
	    }
	    //get list of public holidays for the selected calendar - ends
	    
	    //validating the calculated date is public holiday - start
	    Calendar finalDate=checkHoliday(holidayList, cal);
	    calculateDate = finalDate.getTime();
	    //validating the calculated date is public holiday - End	
	    
	    finalDate=checkHoliday(holidayList, cal);
	    calculateDate = finalDate.getTime();
	    
	    obj.put("calculatedDate", new SimpleDateFormat(LabelConstants.timeFormat).format(calculateDate.getTime()));
	        
	    return obj;
	}
	
	public Integer getValidityPeriod(String calendarId){
		Integer validityPeriod=0;
		List<com.singX.model.Calendar>calendarsList=calendarDAO.getCalendars(calendarId);
		for(com.singX.model.Calendar calendar:calendarsList){
			validityPeriod=calendar.getValidityPeriod();
			break;
		}
		return validityPeriod;
	}
	
	/*public String getWeekendDetails(String calendarId){
		String weekend="";
		List<com.singX.model.Calendar>calendarsList=calendarDAO.getCalendars(Integer.parseInt(calendarId));
		for(com.singX.model.Calendar calendar:calendarsList){
			weekend=calendar.getWeekends();
			break;
		}
		return weekend;
	}*/
	
	public JSONObject bookSlotVerification(String customerId,String verifierId,String fromDate,String toDate,String calendarId,String location) throws ParseException{
		JSONObject json=new JSONObject();
		List<VerifcationDetails> slotDetailsByVerifier= new ArrayList<VerifcationDetails>();
		List<VerifcationDetails> slotDetailsByCustomer= new ArrayList<VerifcationDetails>();
		List<String>status=new ArrayList<String>();
		List<String>cusStatus=new ArrayList<String>();
		List<String>verStatus=new ArrayList<String>();
		slotDetailsByVerifier=verificationDetailsDAO.getVerifcationDetails(verifierId,Util.convertdateTimetoDate(fromDate));
		slotDetailsByCustomer=verificationDetailsDAO.getCustomerVerifcationDetails(customerId,Util.convertdateTimetoDate(fromDate));
		
			for(VerifcationDetails slots:slotDetailsByVerifier){
				SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			    Date selfromDate=sdf2.parse(slots.getScheduledFrom().toString());
			    
			    SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			    Date selToDate=sdf3.parse(slots.getScheduledTo().toString());
				
			    status.add(checkAvailablity(fromDate, toDate, selfromDate, selToDate));
			    verStatus.add(checkAvailablity(fromDate, toDate, selfromDate, selToDate));
			}
			
			//
			for(VerifcationDetails slots:slotDetailsByCustomer){
				SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			    Date selfromDate=sdf2.parse(slots.getScheduledFrom().toString());
			    
			    SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			    Date selToDate=sdf3.parse(slots.getScheduledTo().toString());
				
				status.add(checkAvailablity(fromDate, toDate, selfromDate, selToDate));
				cusStatus.add(checkAvailablity(fromDate, toDate, selfromDate, selToDate));
			}
			//
		
		if(status.contains("false")){
			json.clear();
			if(cusStatus.contains("false")&&verStatus.contains("false")){
				json.put("message","slot cannot be booked try with some other slots.");
				json.put("bookedStatus", "false");
			}else if(cusStatus.contains("false")){
				json.put("message","customer is booked with another slot.");
				json.put("bookedStatus", "false");
			}else if(verStatus.contains("false")){
				json.put("message","verifier is booked with another slot.");
				json.put("bookedStatus", "false");
			}
			
		}else{
			json.clear();
			List<TimeSlotsAvailability>slotDetails=verificationDetailsDAO.getAvailabilitySlots(verifierId, Util.convertdateTimetoDate(fromDate));
			if(checkAvailability(slotDetails, fromDate, toDate)){
				List<VerifcationDetails>registeredDetails=verificationDetailsDAO.save(setVerificationDetails(customerId, verifierId, fromDate, toDate, calendarId,location));
				json=Util.convertJSON(registeredDetails);
			}else{
				json.put("message","verifier is not available.");
				json.put("bookedStatus", "false");
			}
		}
		return json;
	}
	
	public String checkAvailablity(String selectedFrom,String SelectedTo,Date regFromDate,Date regToDate) throws ParseException{
		boolean selectedDatefromStatus;
		boolean selectedDatetoStatus;
		boolean finalStatus;
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Date selfromDate=sdf2.parse(selectedFrom);
	    
	    SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Date selToDate=sdf3.parse(SelectedTo);
	    
	    //
	    if(selfromDate.after(regFromDate)&&selfromDate.before(regToDate)){
	    	selectedDatefromStatus= false;
	    }else if(selfromDate.equals(regFromDate)&&selToDate.equals(regToDate)){
	    	selectedDatefromStatus= false;
	    }else{
	    	selectedDatefromStatus= true;
	    }
	    
	    if(selToDate.after(regFromDate)&&selToDate.before(regToDate)){
	    	selectedDatetoStatus= false;
	    }else if(selfromDate.equals(regFromDate)&&selToDate.equals(regToDate)){
	    	selectedDatetoStatus= false;
	    }else{
	    	selectedDatetoStatus= true;
	    }
	    //
	    
	    finalStatus=selectedDatefromStatus&&selectedDatetoStatus;
	    String status=String.valueOf(finalStatus);
	    return status;
	}
	
	public VerifcationDetails setVerificationDetails(String customerId,String verifierId,String fromDate,String toDate,String calendarId,String location){
		VerifcationDetails slotDetails=new VerifcationDetails();
		slotDetails.setCustomerId(customerId);
		slotDetails.setVerifierId(verifierId);
		slotDetails.setScheduledFrom(Timestamp.valueOf(fromDate));
		slotDetails.setScheduledTo(Timestamp.valueOf(toDate));
		slotDetails.setCalendarId(calendarId);
		slotDetails.setLocation(location);
		slotDetails.setIsValidRec(1);
		return slotDetails;
	}
	
	
	public JSONArray getDates(String verifierId,String fromDate,String toDate,String calendarId) throws ParseException{
		JSONArray jsonArray=new JSONArray();
		JSONObject finalJson=new JSONObject();
		JSONObject json=new JSONObject();
		List<String> verifierList=new ArrayList<String>();
		Set<String>verifierSet=new HashSet<String>();
		Map<String,List<String>>allSlots=new HashMap<String, List<String>>();
		
		List<String> availableSlots=new ArrayList<String>();
	    List<TimeSlotsAvailability>data=verificationDetailsDAO.getAvailabilityByVerifier(verifierId,fromDate,toDate,calendarId);
		for(TimeSlotsAvailability slot:data){
			Date date = new Date(slot.getAvailableFrom().getTime());
			verifierList.add(slot.getVerifierId().toString());
			availableSlots=insideTesting(slot);
			String key=slot.getVerifierId().toString()+"/"+new SimpleDateFormat("yyyy-MM-dd").format(date.getTime());
			if(allSlots.containsKey(key)){
				List<String>previousList=new ArrayList<String>();
				List<String> listFinal=new ArrayList<String>();
				previousList=allSlots.get(key);
				listFinal.addAll(previousList);
				if(availableSlots.size()>0){
					listFinal.addAll(availableSlots);
				}
				allSlots.put(key, listFinal);
			}else{
				if(availableSlots.size()>0){
					allSlots.put(key, availableSlots);
				}
			}
		}
		verifierSet.addAll(verifierList);
		verifierList.clear();
		verifierList.addAll(verifierSet);
		
		for(String vId:verifierList){
			json.clear();
			finalJson.clear();
			for(Map.Entry<String,List<String>>entry:allSlots.entrySet()){
				if(entry.getKey().startsWith(vId)){
					String[] str=entry.getKey().split("/");
					json.put(str[1],entry.getValue());
				}
			}
			finalJson.put("verifierId", vId);
			finalJson.put("availableDates", json);
			jsonArray.add(finalJson);
		}
		return jsonArray;
	}
	
	public boolean checkAvailability(List<TimeSlotsAvailability> details,String selectedFrom,String SelectedTo) throws ParseException{
		String status="";
		List<String>statusDetails=new ArrayList<String>();
		for(TimeSlotsAvailability availablity:details){
			
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    Date selfromDate=sdf2.parse(availablity.getAvailableFrom().toString());
		    
		    SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    Date selToDate=sdf3.parse(availablity.getAvailableTo().toString());
		    
		    status=checkAvailablity(selectedFrom, SelectedTo, selfromDate, selToDate);
			statusDetails.add(status);
		}
		if(statusDetails.contains("false")){
			return true;
		}else{
			return false;
		}
	}
	
	public List<String> insideTest(TimeSlotsAvailability availableDetails) throws ParseException{
		Date date = new Date(availableDetails.getAvailableFrom().getTime());
		List<String>datas=new ArrayList<String>();
		
	    SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Date avlfromDate=sdf2.parse(availableDetails.getAvailableFrom().toString());
	    
	    SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Date avlToDate=sdf3.parse(availableDetails.getAvailableTo().toString());
	    	    	    
	    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
	    String avlfromTime=timeFormat.format(avlfromDate);
		String avltoTime=timeFormat.format(avlToDate);
 
	    String first=avlfromTime;
	    String concatedString="";
	    List<VerifcationDetails>bookedSlots=verificationDetailsDAO.getVerifcationDetails(availableDetails.getVerifierId(),new SimpleDateFormat("yyyy-MM-dd").format(date.getTime()));
		for(VerifcationDetails regDetail:bookedSlots){
			SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    Date selfromDate=sdf4.parse(regDetail.getScheduledFrom().toString());
		    
		    SimpleDateFormat sdf5 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    Date selToDate=sdf5.parse(regDetail.getScheduledTo().toString());
		    
			if(selfromDate.after(avlfromDate) && selToDate.before(avlToDate)){
				String fromTime=timeFormat.format(selfromDate);
				String toTime=timeFormat.format(selToDate);
				
				concatedString=first+"-"+fromTime;
				datas.add(concatedString);
				first=toTime;
			}
		}
		concatedString=first+"-"+avltoTime;
		datas.add(concatedString);
		return datas;
	}
	
	//
	public List<String> insideTesting(TimeSlotsAvailability availableDetails) throws ParseException{
		Date date = new Date(availableDetails.getAvailableFrom().getTime());
		List<String>datas=new ArrayList<String>();
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date avlfromDate=sdf2.parse(availableDetails.getAvailableFrom().toString());
		    
		SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date avlToDate=sdf3.parse(availableDetails.getAvailableTo().toString());
	    	    	    
	    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
	    String avlfromTime=timeFormat.format(avlfromDate);
		String avltoTime=timeFormat.format(avlToDate);
 
	    String first=avlfromTime;
	    String concatedString="";
	    List<VerifcationDetails>bookedSlots=verificationDetailsDAO.getVerifcationDetails(availableDetails.getVerifierId(),new SimpleDateFormat("yyyy-MM-dd").format(date.getTime()));
		for(VerifcationDetails regDetail:bookedSlots){
			SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    Date selfromDate=sdf4.parse(regDetail.getScheduledFrom().toString());
		    
		    SimpleDateFormat sdf5 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    Date selToDate=sdf5.parse(regDetail.getScheduledTo().toString());
		    
		    //
		    if(selfromDate.getTime()==avlfromDate.getTime()){
				first=timeFormat.format(selToDate);
			}
		    if(selToDate.getTime()==avlToDate.getTime()){
				first=timeFormat.format(selfromDate);
			}
		    //
			if(selfromDate.after(avlfromDate) && selToDate.before(avlToDate)){
				String fromTime=timeFormat.format(selfromDate);
				String toTime=timeFormat.format(selToDate);
				//
				if(selfromDate.getTime()==avlfromDate.getTime()){
					first=toTime;
				}
				//
				concatedString=first+"-"+fromTime;
				datas.add(concatedString);
				first=toTime;
			}
		}
		concatedString=first+"-"+avltoTime;
		datas.add(concatedString);
		datas=getDividedSlots(datas,new SimpleDateFormat("yyyy-MM-dd").format(date.getTime()));
		return datas;
	}
	//
	
	public boolean insideTest(String verfierId,Date fromDate,Date toDate) throws ParseException{
		List<String>slotStatus=new ArrayList<String>();
	    List<VerifcationDetails>bookedSlots=verificationDetailsDAO.getVerifcationDetails(verfierId,new SimpleDateFormat("yyyy-MM-dd").format(fromDate.getTime()));
		for(VerifcationDetails regDetail:bookedSlots){
			boolean fromStatus;
			boolean toStatus;
			boolean calculatedStatus;
			boolean availabilityCheck;
			boolean finalStatus;
			SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    Date selfromDate=sdf4.parse(regDetail.getScheduledFrom().toString());
		    
		    SimpleDateFormat sdf5 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    Date selToDate=sdf5.parse(regDetail.getScheduledTo().toString());
		    
		    String toDateTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(toDate.getTime());
		    String fromDateTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fromDate.getTime());
		    
		    String selFromDateTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(selfromDate.getTime());
		    String selToDateTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(selToDate.getTime());		
		    
			if(fromDate.after(selfromDate) && fromDate.before(selToDate)){
				fromStatus=false;
			}else if(fromDateTime.equalsIgnoreCase(selFromDateTime) 
					&& toDateTime.equalsIgnoreCase(selToDateTime)){
				fromStatus=false;
			}else{
				fromStatus=true;
			}
			if(toDate.after(selfromDate) && toDate.before(selToDate)){
				toStatus=false;
			}else if(fromDateTime.equalsIgnoreCase(selFromDateTime) 
					&& toDateTime.equalsIgnoreCase(selToDateTime)){
				toStatus=false;
			}else{
				toStatus=true;
			}
			calculatedStatus=fromStatus&&toStatus;
			//availabilityCheck=finalCheckAvailability(fromDate, toDate, verfierId);
		///	finalStatus=calculatedStatus&&availabilityCheck;
			String status=String.valueOf(calculatedStatus);
			slotStatus.add(status);
		}
		if(slotStatus.contains("false")){
			return false;
		}else{
			return true;
		}
	}
	
	public JSONObject checkSlots(String verifierId,String fromDate,String toDate,String calendarId) throws ParseException{	
		JSONArray verifiersArray=new JSONArray();		
		JSONObject json=new JSONObject();
		
		List<String>verifiersList=new ArrayList<String>();
		Set<String>verifiersSet=new HashSet<String>();
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Date selfromDate=sdf2.parse(fromDate);
	    
	    SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Date selToDate=sdf3.parse(toDate);
		
	    List<TimeSlotsAvailability>data=verificationDetailsDAO.getAvailabilityByVerifier(verifierId,
	    		new SimpleDateFormat("yyyy-MM-dd").format(selfromDate.getTime()),
	    		new SimpleDateFormat("yyyy-MM-dd").format(selToDate.getTime()),calendarId);
	    
		for(TimeSlotsAvailability slot:data){
			//Bug fix schedule meeting -2016-12-22 - start
			boolean finalstatus=false;
			boolean fromstatus=false;
			boolean tostatus=false;
			String fromDateTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(slot.getAvailableFrom().getTime());
			String toDateTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(slot.getAvailableTo().getTime());
			if(selfromDate.after(slot.getAvailableFrom()) && selfromDate.before(slot.getAvailableTo())){
				fromstatus=true;
			}else if(fromDateTime.equalsIgnoreCase(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(selfromDate.getTime()))){
				fromstatus=true;
			}
			if(selToDate.after(slot.getAvailableFrom()) && selToDate.before(slot.getAvailableTo())){
				tostatus=true;
			}else if(toDateTime.equalsIgnoreCase(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(selToDate.getTime()))){
				tostatus=true;
			}
			finalstatus=fromstatus&&tostatus;
			//Bug fix schedule meeting -2016-12-22 - end
			
			if(finalstatus){
				verifiersList.add(slot.getVerifierId());
			}
		}
		
		verifiersSet.addAll(verifiersList);
		verifiersList.clear();
		verifiersList.addAll(verifiersSet);
		
		for(String vid:verifiersList){
			boolean status=insideTest(vid, selfromDate, selToDate);
			if(status){
				verifiersArray.add(vid);
			}
		}
		json.put("verifierId",verifiersArray);
		if(verifiersArray.size()>0){
			json.put("slotAvailable",true);
		}else{
			json.put("slotAvailable",false);
		}
		json.put("selectedFromDateTime",fromDate);
		json.put("selectedToDateTime",toDate);
		
		return json;
	}
	
	public boolean finalCheckAvailability(Date fromDate,Date toDate,String verifierId) throws ParseException{
		List<TimeSlotsAvailability>data=verificationDetailsDAO.getAvailabilityByVerifierId(verifierId,
	    		new SimpleDateFormat("yyyy-MM-dd").format(fromDate.getTime()),
	    		new SimpleDateFormat("yyyy-MM-dd").format(toDate.getTime()));
		List<String>slotStatus=new ArrayList<String>();
		for(TimeSlotsAvailability available:data){
			boolean fromStatus;
			boolean toStatus;
			boolean finalStatus;
			SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    Date selfromDate=sdf4.parse(available.getAvailableFrom().toString());
		    
		    SimpleDateFormat sdf5 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    Date selToDate=sdf5.parse(available.getAvailableTo().toString());
		    
		    String toDateTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(toDate.getTime());
		    String fromDateTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fromDate.getTime());
		    
		    String selFromDateTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(selfromDate.getTime());
		    String selToDateTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(selToDate.getTime());		
		    
		    if(fromDate.after(selfromDate) && fromDate.before(selToDate)){
				fromStatus=false;
			}else if(fromDateTime.equalsIgnoreCase(selFromDateTime) 
					&& toDateTime.equalsIgnoreCase(selToDateTime)){
				fromStatus=false;
			}else{
				fromStatus=true;
			}
			if(toDate.after(selfromDate) && toDate.before(selToDate)){
				toStatus=false;
			}else if(fromDateTime.equalsIgnoreCase(selFromDateTime) 
					&& toDateTime.equalsIgnoreCase(selToDateTime)){
				toStatus=false;
			}else{
				toStatus=true;
			}
			finalStatus=fromStatus&&toStatus;
			String status=String.valueOf(finalStatus);
			slotStatus.add(status);
		}
		if(slotStatus.contains("false")){
			return false;
		}else{
			return true;
		}
		//return false;
	}
	
	
	//get slot by filter
	public JSONArray getSlotByVerifier(String fromDate,String toDate,String filterBy,String customerId,String verifierId) throws ParseException{	
		List<VerifcationDetails>bookedSlots=null;
		JSONArray timeArray=new JSONArray();	
		JSONArray locationArray=new JSONArray();	
		JSONArray finalArray=new JSONArray();	
		JSONObject json=new JSONObject();
		JSONObject bookedDates=new JSONObject();
		List<String>idList=new ArrayList<String>();
		Set<String>idSet=new HashSet<String>();
		if(filterBy.equalsIgnoreCase("verifier") && filterBy!=null){
			bookedSlots=verificationDetailsDAO.getAllScheduledVerifiers(fromDate,toDate,verifierId);
		}else if(filterBy.equalsIgnoreCase("customer") && filterBy!=null){
			bookedSlots=verificationDetailsDAO.getAllScheduledCustomers(fromDate,toDate,customerId);
		}else{
			bookedSlots=verificationDetailsDAO.getAllScheduled(fromDate,toDate);
		}
		for(VerifcationDetails details:bookedSlots){
			if(filterBy.equalsIgnoreCase("verifier")){
				idList.add(details.getVerifierId());
			}else if(filterBy.equalsIgnoreCase("customer")){
				idList.add(details.getCustomerId());
			}else{
				idList.add(details.getVerifierId());
			}
		}
		
		idSet.addAll(idList);
		idList.clear();
		idList.addAll(idSet);
		Collections.sort(idList);
		
		 SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
		 String currentDateTime="";
		 String previousDateTime="";
		for(String id:idList){
			List<VerifcationDetails>datas=new ArrayList<VerifcationDetails>();
			json.clear();
			bookedDates.clear();
			timeArray.clear();
			locationArray.clear();
			if(filterBy.equalsIgnoreCase("verifier")){
				datas=verificationDetailsDAO.getVerifcationDetails(id, fromDate, toDate);
				json.put("verifierId", id);
			}else if(filterBy.equalsIgnoreCase("customer")){
				datas=verificationDetailsDAO.getCustomerVerifcationDetails(id, fromDate, toDate);
				json.put("customerId", id);
			}else{
				datas=verificationDetailsDAO.getVerifcationDetails(id, fromDate, toDate);
				json.put("verifierId", id);
			}
			//json.put("verifierId", id);
			for(VerifcationDetails bookedDetails:datas){
				SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			    Date selfromDate=sdf4.parse(bookedDetails.getScheduledFrom().toString());
			    
			    SimpleDateFormat sdf5 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			    Date selToDate=sdf5.parse(bookedDetails.getScheduledTo().toString());
			    
			    String fromTime=timeFormat.format(selfromDate);
			    String toTime=timeFormat.format(selToDate);
			    
			    currentDateTime=new SimpleDateFormat("yyyy-MM-dd").format(selfromDate.getTime());
			    if(currentDateTime.equalsIgnoreCase(previousDateTime)){
			    	timeArray.add(fromTime+"-"+toTime);
			    	bookedDates.put(new SimpleDateFormat("yyyy-MM-dd").format(selfromDate.getTime()), timeArray);
			    	json.put("bookedDates", bookedDates);
			    	//29-11-2016 - start
			    	//json.put("location/"+currentDateTime+"/"+fromTime+"-"+toTime, bookedDetails.getLocation());
			    	//29-11-2016 - end
			    }else{
			    	timeArray.clear();
			    	timeArray.add(fromTime+"-"+toTime);
			    	bookedDates.put(new SimpleDateFormat("yyyy-MM-dd").format(selfromDate.getTime()), timeArray);
			    	json.put("bookedDates", bookedDates);
			    	//29-11-2016 - start
			    	//json.put("location/"+currentDateTime+"/"+fromTime+"-"+toTime, bookedDetails.getLocation());
			    	//29-11-2016 - end
			    }
			    previousDateTime=currentDateTime;
			   // json.put("location-"+currentDateTime, bookedDetails.getLocation());
			}
			json.put("bookedDates", bookedDates);
			bookedDates.clear();
			
			//29-11-2016 - start
			for(VerifcationDetails bookedDetails:datas){
				SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			    Date selfromDate=sdf4.parse(bookedDetails.getScheduledFrom().toString());
			    
			    SimpleDateFormat sdf5 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			    Date selToDate=sdf5.parse(bookedDetails.getScheduledTo().toString());
			    
			    String fromTime=timeFormat.format(selfromDate);
			    String toTime=timeFormat.format(selToDate);
			    
			    currentDateTime=new SimpleDateFormat("yyyy-MM-dd").format(selfromDate.getTime());
			    if(currentDateTime.equalsIgnoreCase(previousDateTime)){
			    	locationArray.add(bookedDetails.getLocation());
			    	bookedDates.put(new SimpleDateFormat("yyyy-MM-dd").format(selfromDate.getTime()), locationArray);
			    	json.put("location", bookedDates);
			    	//json.put("location/"+currentDateTime+"/"+fromTime+"-"+toTime, bookedDetails.getLocation());
			    }else{
			    	locationArray.clear();
			    	locationArray.add(bookedDetails.getLocation());
			    	bookedDates.put(new SimpleDateFormat("yyyy-MM-dd").format(selfromDate.getTime()), locationArray);
			    	json.put("location", bookedDates);
			    	//json.put("location/"+currentDateTime+"/"+fromTime+"-"+toTime, bookedDetails.getLocation());
			    }
			    previousDateTime=currentDateTime;
			   // json.put("location-"+currentDateTime, bookedDetails.getLocation());
			}
			
			//29-11-2016 -end
			
			json.put("location", bookedDates);
			finalArray.add(json);
		}
		
		return finalArray;
	}
	//get slot by filter
	
	//Calendar API - end
	
	//Calendar admin screen - start
	public String calendarRegistration(String calendarID,String calendarName,String validityPeriod,String countryCode,String countryName){
			com.singX.model.Calendar calendar=new com.singX.model.Calendar();
			calendar.setCalendarId(calendarID);
			calendar.setCalendarName(calendarName);
			calendar.setValidityPeriod(Integer.parseInt(validityPeriod));
			calendar.setCountryCode(countryCode);
			calendar.setCountryName(countryName);
			calendar.setIsValidRec(1);
			calendarDAO.saveCalendar(calendar);
		return "";
	}
	
	public String calendarSave(String calendarID,String calendarName,String validityPeriod,String countryCode,String countryName){
		try{
			com.singX.model.Calendar calendar=new com.singX.model.Calendar();
			calendar.setCalendarId(calendarID);
			calendar.setCalendarName(calendarName);
			calendar.setValidityPeriod(Integer.parseInt(validityPeriod));
			calendar.setCountryCode(countryCode);
			calendar.setCountryName(countryName);
			calendar.setIsValidRec(1);
			calendarDAO.save(calendar);
		}catch(Exception e){
			System.out.println("Calendar already exists");
		}
		return "";
	}
	
	public String calendarDelete(String calendarID,String calendarName,String validityPeriod,String countryCode,String countryName){
		com.singX.model.Calendar calendar=new com.singX.model.Calendar();
		calendar.setCalendarId(calendarID);
		calendar.setCalendarName(calendarName);
		calendar.setValidityPeriod(Integer.parseInt(validityPeriod));
		calendar.setCountryCode(countryCode);
		calendar.setCountryName(countryName);
		calendar.setIsValidRec(0);
		calendarDAO.saveCalendar(calendar);
		return "";
	}
	
	public Map<String,String> getCalendars(){
		Map<String,String>country=new HashMap<String, String>();
		List<com.singX.model.Calendar>calendarsList=calendarDAO.getAllCalendars();
		for(com.singX.model.Calendar calendar:calendarsList){
			country.put(calendar.getCalendarId().toString(), calendar.getCalendarName());
		}
		return country;
	}
	
	public List<com.singX.model.Calendar> getAllCalendars(){
		//Map<String,String>country=new HashMap<String, String>();
		List<com.singX.model.Calendar>calendarsList=calendarDAO.getAllCalendars();
		/*for(com.singX.model.Calendar calendar:calendarsList){
			country.put(calendar.getCalendarId().toString(), calendar.getCalendarName());
		}*/
		return calendarsList;
	}
	//Calendar admin screen - end
	
	
	//2016-11-03 changes made as per discussion with atul
	@SuppressWarnings("deprecation")
	public List<String> getDividedSlots(List<String> globalSlot,String fromdate) throws ParseException{
		List<String> slots=new ArrayList<String>();
		// TODO Auto-generated method stub
		for(String timeValue:globalSlot){
			System.out.println(timeValue);
		String time=timeValue;
		String[] times=time.split("-");
		String fromtime=times[0];
		String toTime=times[1];
		
		//for current time functionality issue 17-11-2016 -start
		//Calendar calendarSys = Calendar.getInstance();
		
		//SGT
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    formatter.setTimeZone(TimeZone.getTimeZone("Asia/Singapore"));
	    Date date1= new Date();  
	    String dateString = formatter.format(date1);    	    
	    SimpleDateFormat sgtFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Date sgtTime=sgtFormat.parse(dateString);
		//SGT
		//Date d1 = new Date();
		String sysDate=new SimpleDateFormat("yyyy-MM-dd").format(sgtTime);
		
		//Calendar cal1 = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		Date sysTime=sdf.parse(sdf.format(sgtTime.getTime()));
		System.out.println("Systime ==>"+sysTime);
	    //for current time functionality issue 17-11-2016 -end
		
		SimpleDateFormat sdf5 = new SimpleDateFormat("HH:mm");
	    Date selToDate=sdf5.parse(fromtime);
	    
	  //  int startTime=Integer.parseInt(toDateTime);
	    
	    SimpleDateFormat sdf6 = new SimpleDateFormat("HH:mm");
	    Date selToDate1=sdf6.parse(toTime);
	    
	    Calendar start = Calendar.getInstance();
	    start.setTime(selToDate);
	    Calendar end = Calendar.getInstance();
	    end.setTime(selToDate1);
	    
	    if(fromdate.equalsIgnoreCase(sysDate)){
	    	for (Date date = start.getTime(); start.before(end); start.add(Calendar.MINUTE, 30), date = start.getTime()) {
		        String fromTimeSlot=new SimpleDateFormat("HH:mm").format(date.getTime());
		        //Date fromTime=sdf6.parse(fromTimeSlot);
		        Date fromTime=new Date();
		        String[] splits=fromTimeSlot.split(":");
		        fromTime.setHours(Integer.parseInt(splits[0]));
		        fromTime.setMinutes(Integer.parseInt(splits[1]));
		        fromTime.setSeconds(0);
			    Calendar cal = Calendar.getInstance(); 
			    cal.setTime(date);
			    cal.add(Calendar.MINUTE, 30);
			    
			    String toTimeSlot=new SimpleDateFormat("HH:mm").format(cal.getTime());
			    
			    String concat=fromTimeSlot+"-"+toTimeSlot;
			    
			    if(fromTime.after(sgtTime)){
			    	slots.add(concat);
			    }
			    /*slots.add("elses-if");
		        slots.add("fromdate==>"+fromdate);
		        slots.add("fromTime==>"+fromTime);
		        slots.add("d1"+sgtTime);
		        slots.add("sysDate==>"+sysDate);
		        slots.add("elses-if");*/
		    }
	    }else{
	    	for (Date date = start.getTime(); start.before(end); start.add(Calendar.MINUTE, 30), date = start.getTime()) {
		        String fromTimeSlot=new SimpleDateFormat("HH:mm").format(date.getTime());
		        
			    Calendar cal = Calendar.getInstance(); 
			    cal.setTime(date);
			    cal.add(Calendar.MINUTE, 30);
			    
			    String toTimeSlot=new SimpleDateFormat("HH:mm").format(cal.getTime());
			    
			    String concat=fromTimeSlot+"-"+toTimeSlot;
			    slots.add(concat);
		        /*slots.add("elses-start");
		        slots.add("from==>"+fromdate);
		        slots.add("sysDate==>"+sysDate);
		        slots.add("elses-end");*/
		    }
	    }
	    
		}
	    return slots;
	}
	//2016-11-03 changes made as per discussion with atul
}
