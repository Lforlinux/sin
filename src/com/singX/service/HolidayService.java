package com.singX.service;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singX.dao.CalendarDAO;
import com.singX.dao.HolidayDAO;
import com.singX.model.Calendar;
import com.singX.model.HolidayList;
import com.singX.util.Util;

@Service
public class HolidayService {
	
	@Autowired
	public HolidayDAO holidayDAO;
	
	@Autowired
	public CalendarDAO calendarDAO;
	
	public String addHolidaysIntoCalendar(String calendarId,String holidayDate,String description) throws ParseException{
		HolidayList holiday=new HolidayList();
		holiday.setCalendarId(calendarId);
		holiday.setHolidayDate(Util.getDatefromString(holidayDate));
		holiday.setDescription(description);
		holiday.setIsValidRec(1);
		holidayDAO.saveHolidays(holiday);
		return "";
	}
	
	public String updateHolidaysIntoCalendar(String holidayId,String calendarId,String holidayDate,String description) throws ParseException{
		HolidayList holiday=new HolidayList();
		holiday.setHolidayId(Integer.parseInt(holidayId));
		holiday.setCalendarId(calendarId);
		holiday.setHolidayDate(Util.getDatefromString(holidayDate));
		holiday.setDescription(description);
		holiday.setIsValidRec(1);
		holidayDAO.saveHolidays(holiday);
		return "";
	}
	
	public String deleteHolidaysIntoCalendar(String holidayId,String calendarId,String holidayDate,String description) throws ParseException{
		HolidayList holiday=new HolidayList();
		holiday.setHolidayId(Integer.parseInt(holidayId));
		holiday.setCalendarId(calendarId);
		holiday.setHolidayDate(Util.getDatefromString(holidayDate));
		holiday.setDescription(description);
		holiday.setIsValidRec(0);
		holidayDAO.saveHolidays(holiday);
		return "";
	}
	
	public Map<String,String> getCalendars(){
		Map<String,String>country=new HashMap<String, String>();
		List<Calendar>calendarsList=calendarDAO.getAllCalendars();
		for(Calendar calendar:calendarsList){
			country.put(calendar.getCalendarId().toString(), calendar.getCalendarName());
		}
		return country;
	}
	
	public List<HolidayList> getAllCalendars(){
		//Map<String,String>country=new HashMap<String, String>();
		List<HolidayList>calendarsList=holidayDAO.getAllHoliday();
		/*for(com.singX.model.Calendar calendar:calendarsList){
			country.put(calendar.getCalendarId().toString(), calendar.getCalendarName());
		}*/
		return calendarsList;
	}
}
