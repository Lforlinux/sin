package com.singX.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singX.dao.CalendarDAO;
import com.singX.dao.VerificationDetailsDAO;
import com.singX.model.Calendar;
import com.singX.model.Singxcontacts;
import com.singX.model.TimeSlotsAvailability;
import com.singX.model.VerifcationDetails;
import com.singX.model.VerifierDetails;

@Service
public class VerficationDetailsService {

	@Autowired
	public VerificationDetailsDAO verificationDetailsDAO;
	
	@Autowired
	public CalendarDAO calendarDAO;  
	
	/*public String checkStatus(String Date) throws ParseException{
		String status="";
		SimpleDateFormat sdf2 = new SimpleDateFormat(LabelConstants.timeFormat);
	    Date selDate=sdf2.parse(Date);
		return status;
	}*/
	
	/*public void bookSlotVerification(String customerId,String verifierId,String fromDate,String toDate){
		List<VerifcationDetails> slotDetails= new ArrayList<VerifcationDetails>();
		slotDetails=verificationDetailsDAO.getVerifcationDetails(Integer.parseInt(verifierId), Integer.parseInt(customerId), toDate);
		System.out.println("size===>"+slotDetails.size());
	}*/
	
	public String saveVerifier(String verifierID,String verifierName,String location){
		VerifierDetails details=new VerifierDetails();
		details.setVerifierId(verifierID);
		details.setVerifierName(verifierName);
		details.setCountryCode(location);
		details.setIsValidRec(1);
		verificationDetailsDAO.saveVerifier(details);
		return "";
	}
	
	public String deleteVerifier(String verifierID,String verifierName,String location){
		VerifierDetails details=new VerifierDetails();
		details.setVerifierId(verifierID);
		details.setVerifierName(verifierName);
		details.setCountryCode(location);
		details.setIsValidRec(0);
		verificationDetailsDAO.saveVerifier(details);
		return "";
	}
	
	public JSONObject saveTimeSlotAvailability(String verifierId,String calendarId,Timestamp fromTime,
			Timestamp toTime,String date,String fromDateTime,String toDateTime) throws ParseException{
		JSONObject json=new JSONObject();
		boolean status=checkAvailability(verifierId, date, fromDateTime, toDateTime);
		if(status){
			TimeSlotsAvailability availability=new TimeSlotsAvailability();
			availability.setVerifierId(verifierId);
			availability.setCalendarId(calendarId);
			availability.setAvailableFrom(fromTime);
			availability.setAvailableTo(toTime);
			availability.setIsValidRec(1);
			verificationDetailsDAO.saveAvailability(availability);
			json.put("status","true");
		}else{
			json.put("status","false");
		}
		return json;
	}
	
	public JSONObject updateTimeSlotAvailability(String verifierId,String calendarId,String availabilityId,Timestamp fromTime,
			Timestamp toTime,String date,String fromDateTime,String toDateTime) throws ParseException{
		JSONObject json=new JSONObject();
		//boolean status=checkAvailability(verifierId, date, fromDateTime, toDateTime);
		//if(status){
			TimeSlotsAvailability availability=new TimeSlotsAvailability();
			availability.setAvailabilityId(Integer.parseInt(availabilityId));
			availability.setVerifierId(verifierId);
			availability.setCalendarId(calendarId);
			availability.setAvailableFrom(fromTime);
			availability.setAvailableTo(toTime);
			availability.setIsValidRec(1);
			verificationDetailsDAO.saveAvailability(availability);
			json.put("status","true");
		//}else{
		//	json.put("status","false");
		//}
		return json;
	}
	
	public JSONObject updateSlotAvailability(String verifierId,String calendarId,String scheduledId,String customerId,Timestamp fromTime,
			Timestamp toTime,String date,String fromDateTime,String toDateTime) throws ParseException{
			boolean status=checkValid(verifierId, calendarId, scheduledId, date, fromDateTime, toDateTime);
			JSONObject json=new JSONObject();
			if(status){
				VerifcationDetails slots=new VerifcationDetails();
				slots.setScheduledId(Integer.parseInt(scheduledId));
				slots.setVerifierId(verifierId);
				slots.setCalendarId(calendarId);
				slots.setCustomerId(customerId);
				slots.setScheduledFrom(fromTime);
				slots.setScheduledTo(toTime);
				slots.setIsValidRec(1);
				verificationDetailsDAO.update(slots);
				json.put("status","true");
			}else{
				json.put("status","false");
			}
			
		//}else{
		//	json.put("status","false");
		//}
		return json;
	}
	
	public JSONObject deleteSlotAvailability(String verifierId,String calendarId,String scheduledId,String customerId,Timestamp fromTime,
			Timestamp toTime,String date,String fromDateTime,String toDateTime) throws ParseException{
		JSONObject json=new JSONObject();
			VerifcationDetails slots=new VerifcationDetails();
			slots.setScheduledId(Integer.parseInt(scheduledId));
			slots.setVerifierId(verifierId);
			slots.setCalendarId(calendarId);
			slots.setCustomerId(customerId);
			slots.setScheduledFrom(fromTime);
			slots.setScheduledTo(toTime);
			slots.setIsValidRec(0);
			verificationDetailsDAO.update(slots);
			json.put("status","true");
		//}else{
		//	json.put("status","false");
		//}
		return json;
	}
	
	public JSONObject deleteTimeSlotAvailability(String verifierId,String calendarId,String availabilityId,Timestamp fromTime,
			Timestamp toTime,String date,String fromDateTime,String toDateTime) throws ParseException{
		JSONObject json=new JSONObject();
		//boolean status=checkAvailability(verifierId, date, fromDateTime, toDateTime);
		//if(status){
			TimeSlotsAvailability availability=new TimeSlotsAvailability();
			availability.setAvailabilityId(Integer.parseInt(availabilityId));
			availability.setVerifierId(verifierId);
			availability.setCalendarId(calendarId);
			availability.setAvailableFrom(fromTime);
			availability.setAvailableTo(toTime);
			availability.setIsValidRec(0);
			verificationDetailsDAO.saveAvailability(availability);
			json.put("status","true");
		//}else{
		//	json.put("status","false");
		//}
		return json;
	}
	
	public Map<String,String> getCalendars(){
		Map<String,String>country=new HashMap<String, String>();
		List<Calendar>calendarsList=calendarDAO.getAllCalendars();
		for(Calendar calendar:calendarsList){
			country.put(calendar.getCalendarId().toString(), calendar.getCalendarName());
		}
		return country;
	}
	
	public Map<String,String> getAllVerifiers(){
		Map<String,String>verifiers=new HashMap<String, String>();
		List<VerifierDetails>verifiersList=verificationDetailsDAO.getAllVerifiers();
		for(VerifierDetails verifier:verifiersList){
			verifiers.put(verifier.getVerifierId().toString(), verifier.getVerifierName());
		}
		return verifiers;
	}
	
	public List<VerifcationDetails> getAllVerificationList(){
		List<VerifcationDetails>verifiersList=verificationDetailsDAO.getAllVerificationList();
		return verifiersList;
	}
	
	public List<TimeSlotsAvailability> getAllVerifierAvailability(){
		List<TimeSlotsAvailability> verifiersList=verificationDetailsDAO.getAllVerifierAvailabilityFromNow();
		return verifiersList;
	}
	
	public List<VerifierDetails> getAllVerifierList(){
		//Map<String,String>verifiers=new HashMap<String, String>();
		List<VerifierDetails>verifiersList=verificationDetailsDAO.getAllVerifiers();
		/*for(VerifierDetails verifier:verifiersList){
			verifiers.put(verifier.getVerifierId().toString(), verifier.getVerifierName());
		}*/
		return verifiersList;
	}
	
	public Map<String,String> getHours(){
		Map<String,String>hours=new HashMap<String, String>();
		for(Integer i=0;i<=23;i++){
	    	String hour="";
	    	hour=i.toString();
	    	if(hour.length()==1){
	    		//System.out.println("0"+i);
	    		hours.put("0"+i, "0"+i);
	    	}else{
	    		//System.out.println(i);
	    		hours.put(i.toString(), i.toString());
	    	}
	    }
		Map<String, String> treeMap = new TreeMap<String, String>(hours);
		return treeMap;
	}
	
	public Map<String,String> getMinutes(){
		Map<String,String>minutes=new HashMap<String, String>();
		for(Integer i=0;i<=59;i++){
	    	String hour="";
	    	hour=i.toString();
	    	if(hour.length()==1){
	    		//System.out.println("0"+i);
	    		if(i==0||i==30){
	    			minutes.put("0"+i, "0"+i);
	    		}
	    	}else{
	    		//System.out.println(i);
	    		if(i==0||i==30){
	    			minutes.put(i.toString(), i.toString());
	    		}
	    	}
	    }
		Map<String, String> treeMap = new TreeMap<String, String>(minutes);
		return treeMap;
	}
	
	public boolean checkAvailability(String verifierId,String fromDate,String fromTime,String toTime) throws ParseException{
		//JSONObject json=new JSONObject();
		List<TimeSlotsAvailability>data=verificationDetailsDAO.getAvailabilitySlots(verifierId,fromDate);
		List<String>status=new ArrayList<String>();
		for(TimeSlotsAvailability slot:data){
			String stat=insideSlot(slot,fromTime,toTime);
			status.add(stat);
		}
		if(status.contains("false")){
			//json.put("status", "false");
			return false;
		}else{
			//json.put("status", "true");
			return true;
		}
		//return json;
	}
	
	public boolean checkValid(String verifierId,String calendarId,String scheduledId,String date,String from,String to) throws ParseException{
		List<TimeSlotsAvailability>data=verificationDetailsDAO.getAllAvailabilitySlotByVerifier(verifierId, date, calendarId);
		List<String> availablityStatus=new ArrayList<String>();
		List<String> scheduledStatus=new ArrayList<String>();
		for(TimeSlotsAvailability slot:data){
			boolean selectedDatefromStatus;
			boolean selectedDatetoStatus;
			boolean finalStatus;
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date avlfromDate=sdf2.parse(slot.getAvailableFrom().toString());
			    
			SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date avlToDate=sdf3.parse(slot.getAvailableTo().toString());
			
			SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date fromDate=sdf4.parse(from);
			    
			SimpleDateFormat sdf5 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date toDate=sdf5.parse(to);
			
			if((fromDate.getTime()>=avlfromDate.getTime()) && (fromDate.getTime()<=avlToDate.getTime())){
				selectedDatefromStatus=true;
			}else{
				selectedDatefromStatus=false;
			}
			
			if((toDate.getTime()>=avlfromDate.getTime()) && (toDate.getTime()<=avlToDate.getTime())){
				selectedDatetoStatus=true;
			}else{
				selectedDatetoStatus=false;
			}
			finalStatus=selectedDatefromStatus && selectedDatetoStatus;
			String finalStat=String.valueOf(finalStatus);
			availablityStatus.add(finalStat);
		}
		if(availablityStatus.contains("true")){
			List<VerifcationDetails>verifiersList=verificationDetailsDAO.getAllSlotByVerifier(date, calendarId, verifierId,Integer.parseInt(scheduledId));
			for(VerifcationDetails details:verifiersList){
				boolean selectedDatefromStatus;
				boolean selectedDatetoStatus;
				boolean finalStatus;
				SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date avlfromDate=sdf2.parse(details.getScheduledFrom().toString());
				    
				SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date avlToDate=sdf3.parse(details.getScheduledTo().toString());
				
				SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date fromDate=sdf4.parse(from);
				    
				SimpleDateFormat sdf5 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date toDate=sdf5.parse(to);
				
				if((fromDate.getTime()>=avlfromDate.getTime()) && (fromDate.getTime()<=avlToDate.getTime())){
					selectedDatefromStatus=false;
				}else{
					selectedDatefromStatus=true;
				}
				
				if((toDate.getTime()>=avlfromDate.getTime()) && (toDate.getTime()<=avlToDate.getTime())){
					selectedDatetoStatus=false;
				}else{
					selectedDatetoStatus=true;
				}
				finalStatus=selectedDatefromStatus && selectedDatetoStatus;
				String finalStat=String.valueOf(finalStatus);
				scheduledStatus.add(finalStat);
			}
			if(scheduledStatus.contains("false")){
				return false;
			}else{
				return true;
			}
		}
			
		return false;
	}
	
	public String insideSlot(TimeSlotsAvailability availableDetails,String fromDateTime,String toDateTime) throws ParseException{
		boolean selectedDatefromStatus;
		boolean selectedDatetoStatus;
		boolean finalStatus;
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date avlfromDate=sdf2.parse(availableDetails.getAvailableFrom().toString());
		    
		SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date avlToDate=sdf3.parse(availableDetails.getAvailableTo().toString());
		
		SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date fromDate=sdf4.parse(fromDateTime);
		    
		SimpleDateFormat sdf5 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date toDate=sdf5.parse(toDateTime);
		
		if(fromDate.after(avlfromDate) && fromDate.before(avlToDate)){
			selectedDatefromStatus= false;
		}else if(fromDate.equals(avlfromDate)&&toDate.equals(avlToDate)){
	    	selectedDatefromStatus= false;
	    }else{
	    	selectedDatefromStatus= true;
	    }
		
		if(toDate.after(avlfromDate) && toDate.before(avlToDate)){
			selectedDatetoStatus= false;
		}else if(fromDate.equals(avlfromDate)&&toDate.equals(avlToDate)){
			selectedDatetoStatus= false;
	    }else{
	    	selectedDatetoStatus= true;
	    }
		
		finalStatus=selectedDatefromStatus&&selectedDatetoStatus;
	    String status=String.valueOf(finalStatus);
		return status;
	}
	
	public List<VerifcationDetails> getAllSlots(String fromDate,String calendarId){
		List<VerifcationDetails> slots=verificationDetailsDAO.getAllSlots(fromDate, calendarId);
		List<VerifcationDetails> upadatedSlots=new ArrayList<VerifcationDetails>();
		List<Object[]> datas=verificationDetailsDAO.getAllBookedSlots();
		Map<String,String>contacts=new HashMap<String, String>();
		for(Object ct:datas){
			/*String name=ct.getFirstName()+" "+ct.getLastName();
			contacts.put(ct.getContactId(),name);*/
			Object[] obj = (Object[])ct;
			String name=obj[1]+" "+obj[2];
			contacts.put(obj[0].toString(),name);
			System.out.println(obj[0].toString());
		}
		for(VerifcationDetails details:slots){
			if(contacts.get(details.getCustomerId())!=null && !contacts.get(details.getCustomerId()).isEmpty())
			{
				details.setCustomerName(contacts.get(details.getCustomerId()));
			}
			upadatedSlots.add(details);
		}
		return upadatedSlots;
	}
}
